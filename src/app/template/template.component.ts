import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ServerService} from '../server-service';


@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {


  matchingCrit: string;

  ruleName: string;

  action: string;

  conditionOperator: String[] = ['AND', 'OR'];

  conditionOp: string;

  id: number = 0;

  operators: String[] = ['Equal to', 'Not equal to', 'Less than', 'Greater than', 'Less than equal to', 'Greater than equal to'];

  dataCatlogue: String[] = [
    'bookingNumber',
    'vendor',
    'expectedCargoReceiptDate',
    'expectedCargoReceiptWeek',
    'podCity',
    'podProvince',
    'bookedQuantity',
    'bookedCartons',
    'bookedWeight',
    'bookedCbm',
    'booked volume',
    'shipto',
    'demandKey'];

  actions: String[] = [
    'Order Released',
    'Order on Hold',
    'Order on Exception'
  ];

  conditions: { id: number, dataCtl: string, opearator: string, text: string, condOperator: string }[][] = [
    [{'id': this.id, 'dataCtl': 'vendor', 'opearator': 'Equal to', 'text': '0', 'condOperator': 'AND'}]
  ]

  addItem(val1, val2, textVal, con) {
    this.id = this.id + 1;
    this.conditions.push([{'id': this.id, 'dataCtl': val1, 'opearator': val2, 'text': textVal, 'condOperator': 'AND'}]);
  }

  addItemToOr(andCondition) {
    this.id = this.id + 1;
    andCondition.push({
      'id': this.id,
      'dataCtl': 'bookedWeight',
      'opearator': 'Less than',
      'text': '0',
      'condOperator': 'AND'
    });
  }

  removeChildCondition(childCondition, parentCondition) {
    const filteredConditions: { id: number, dataCtl: string, opearator: string, text: string, condOperator: string }[][] = [];
    for (let entry of this.conditions) {
      if (entry === parentCondition) {
        entry = parentCondition.filter(condition => condition !== childCondition);
      }
      filteredConditions.push(entry);

    }
    this.conditions = filteredConditions;

  }


  removeCondition(condition) {
    this.conditions = this.conditions.filter(cond => condition !== cond);
  }

  onSave() {
    // let headers = new Headers();
    // headers.append('Access-Control-Allow-Origin', 'POST' );
    let body = '{ "ruleName" : ' + '"' + this.ruleName + '"' + ',' + this.matchingCrit + ',' + '"conditionDto" : {' + '"' + 'condition' + '" : ' + '"' + this.getConditionString() + '"' + '}' + ',' + '"actionDto" : [{ ' + '"' + 'actionName' + '" :' + '"' + this.action + '"' + '}]}';
    console.log(body);
    //    console.log(JSON.stringify(body));
    //    let headers = new Headers({ 'Content-Type': 'application/json'});
    //    this.http.post('/rest/createRule', body).subscribe();
    this.serverService.storeRules(body);
  }

  onGet() {
    this.serverService.getRules();
  }

  getConditionString() {
    var condString = '';
    for (let condition of this.conditions) {
      condString = condString.concat(' ( ');
      for (let child of condition) {

        condString = condString.concat(this.getString(child));
        condString = condString.replace('AND', ' & ');
        condString = condString.replace('OR', ' | ');
        //condString = condString.concat(' )');

        //  condString = condString.concat(this.getString(child));
      }
      // //condString = condString.slice(0, condString.lastIndexOf('|'));
      // condString = condString.concat(' ) & ');
    }
    // condString = condString.slice(0, condString.lastIndexOf('&'));

    condString = condString.replace(/.$/, '');
    condString = condString.replace(/.$/, '');
    condString = condString.concat(' )');
    // condString = condString.concat(']');
    return condString;
  }

  getString(condition: { dataCtl: string, opearator: string, text: string, condOperator: string }) {
    return condition ? ' ( ' + condition.dataCtl.concat(' ').concat(condition.opearator).concat(' ').concat(condition.text).concat(' ) ').concat(condition.condOperator) : null;
  }


  constructor(private route: ActivatedRoute, private serverService: ServerService) {

  }

  ngOnInit() {
    let mc = this.route.snapshot.params['mc'];
    this.matchingCrit = mc;
  }
}
