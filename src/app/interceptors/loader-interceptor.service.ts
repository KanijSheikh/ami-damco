
import {Injectable} from '@angular/core';
import {
  HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,
  HttpResponse, HttpResponseBase
} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/do';
import {NgLoadingSpinnerService} from "../loader/loader.service";
import {catchError} from "rxjs/operators";
import {_throw} from "rxjs/observable/throw";
import { of } from 'rxjs/observable/of';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  inProgressRequest = 0;

  constructor(private _loadingBar: NgLoadingSpinnerService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // start our loader here
    this._loadingBar.start();
    this.inProgressRequest = this.inProgressRequest + 1;

    return next.handle(req).do((event: HttpEvent<any>) => {
      // if the event is for http response
      if (event instanceof HttpResponse) {
        // stop our loader here
        this.inProgressRequest = this.inProgressRequest - 1;
        if (this.inProgressRequest <= 0) {
          this._loadingBar.stop();
        }
      }

    }, (err: any) => {
      this.inProgressRequest = this.inProgressRequest - 1;
      if (this.inProgressRequest <= 0) {
        this._loadingBar.stop();
      }
    }).pipe(
      catchError(response => {
          if (response instanceof HttpErrorResponse) {

            // Check if this error has a 2xx status
            if (this.is2xxStatus(response)) {

              // Convert the error to a standard response with a null body and then return
              return of(new HttpResponse({
                headers: response.headers,
                status: response.status,
                statusText: response.statusText,
                url: response.url
              }));
            }

          }

          // This is a real error, rethrow
          return _throw(response);
        }
      ));
  }



  private is2xxStatus(response: HttpResponseBase) {
    return response.status >= 200 && response.status < 300;
  }
}
