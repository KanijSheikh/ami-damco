import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';


import {AppComponent} from './app.component';
import {NavbarComponent} from './navbar/navbar.component';
import {OverviewComponent} from './overview/overview.component';
import {DecisionTreesComponent} from './decision-trees/decision-trees.component';
import {ConfigurationComponent} from './configuration/configuration.component';
import {TemplateComponent} from './template/template.component';
import {MatchingCriteriaComponent} from './matching-criteria/matching-criteria.component';
import {ServerService} from './server-service';
import {OptimisedComponent} from './overview/optimised/optimised.component';
import {CompletedComponent} from './overview/completed/completed.component';
import {ExceptionComponent} from './overview/exception/exception.component';
import {LoadPlanDetailComponent} from './overview/load-plan-detail/load-plan-detail.component';
import {LpOptimisedComponent} from './overview/load-plan-detail/lp-optimised/lp-optimised.component';
import {LpExceptionComponent} from './overview/load-plan-detail/lp-exception/lp-exception.component';
import {LpCarrierBookingComponent} from './overview/load-plan-detail/lp-carrier-booking/lp-carrier-booking.component';
import {PoDropdownComponent} from './overview/load-plan-detail/po-dropdown/po-dropdown.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PoDialogComponent} from './overview/load-plan-detail/po-dialog/po-dialog.component';
import {SchedulesComponent} from './schedules/schedules.component';
import {OrdersComponent} from './orders/orders.component';
import {GlobalService} from './global-service';
import {CreateLoadPlanDialogComponent} from './overview/create-load-plan-dialog/create-load-plan-dialog.component';
import {Origin} from './overview/origin';
import {Destination} from './overview/destination';
import {CountryDataService} from './country-data-service';
import {OriginValues} from './overview/origin-values';
import {DestinationValues} from './overview/destination-values';
import {ExceptionDetailDialogComponent} from './overview/load-plan-detail/exception-detail-dialog/exception-detail-dialog.component';
import {ExceptionAction} from './overview/exception-action';
import {ExceptionActionValues} from './overview/exception-action-values';
import {SimulateDialogComponent} from './overview/load-plan-detail/simulate-dialog/simulate-dialog.component';
import {ActionConfirmedPopupComponent} from './overview/load-plan-detail/action-confirmed-popup/action-confirmed-popup.component';
import {CanDeactivateGuard} from './can-deactivate-guard.service';
import {MyDatePickerModule} from 'mydatepicker';
import {ImportOrdersDialogComponent} from './orders/import-orders-dialog/import-orders-dialog.component';
import {DataTableModule} from 'angular-4-data-table-fix';
import {AddContainerDialogComponent} from './overview/load-plan-detail/lp-optimised/add-container-dialog/add-container-dialog.component';
import {SplitPoDialogComponent} from './overview/load-plan-detail/lp-optimised/split-po-dialog/split-po-dialog.component';
import {NotificationDialogComponent} from './overview/load-plan-detail/notification-dialog/notification-dialog.component';
import {OverviewService} from './overview/overview.service';
import {LoaderInterceptor} from './interceptors/loader-interceptor.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgLoadingSpinnerService} from './loader/loader.service';
import { ScheduleLoadPlanDialogComponent } from './schedules/schedule-load-plan-dialog/schedule-load-plan-dialog.component';
import { ScheduleLoadPlanComponent } from './schedules/schedule-load-plan/schedule-load-plan.component';
import {Ng2MultiSelectDropDownModule} from 'ng-multiselect-dropdown/ng-multiselect-dropdown.module';
import {SortablejsModule} from 'angular-sortablejs';
import {LoadPlanDetailFilterService} from './overview/load-plan-detail/load-plan-detail-filter.service';
import { CreateDecisionTreeDialogComponent } from './decision-trees/create-decision-tree-dialog/create-decision-tree-dialog.component';
import {MyDateRangePickerModule} from 'mydaterangepicker';
import {DataTablesModule} from 'angular-datatables';
import { LocationComponent } from './configuration/location/location.component';
import { AddRecordDialogComponent } from './configuration/add-record-dialog/add-record-dialog.component';
import {ExcelService} from './configuration/excel.service';
import { EditRecordDialogComponent } from './configuration/edit-record-dialog/edit-record-dialog.component';
import {ApplicationDataService} from './configuration/application-data.service';
import { DestinationComponent } from './configuration/destination/destination.component';
import { ApplicationDataComponent } from './configuration/application-data/application-data.component';
import { ContainerFillRateComponent } from './configuration/container-fill-rate/container-fill-rate.component';
import { OriginComponent } from './configuration/origin/origin.component';
import { ContainerTypeComponent } from './configuration/application-data/container-type/container-type.component';
import { VesselScheduleComponent } from './configuration/vessel-schedule/vessel-schedule.component';
import { WarehouseComponent } from './configuration/warehouse/warehouse.component';
import { AllowedContainerComponent } from './configuration/allowed-container/allowed-container.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    OverviewComponent,
    OptimisedComponent,
    CompletedComponent,
    DecisionTreesComponent,
    ConfigurationComponent,
    TemplateComponent,
    MatchingCriteriaComponent,
    ExceptionComponent,
    LoadPlanDetailComponent,
    LpOptimisedComponent,
    LpExceptionComponent,
    LpCarrierBookingComponent,
    PoDropdownComponent,
    PoDialogComponent,
    SchedulesComponent,
    OrdersComponent,
    CreateLoadPlanDialogComponent,
    ExceptionDetailDialogComponent,
    SimulateDialogComponent,
    ActionConfirmedPopupComponent,
    ImportOrdersDialogComponent,
    AddContainerDialogComponent,
    SplitPoDialogComponent,
    NotificationDialogComponent,
    ScheduleLoadPlanDialogComponent,
    ScheduleLoadPlanComponent,
    CreateDecisionTreeDialogComponent,
    LocationComponent,
    AddRecordDialogComponent,
    EditRecordDialogComponent,
    DestinationComponent,
    ApplicationDataComponent,
    ContainerFillRateComponent,
    OriginComponent,
    ContainerTypeComponent,
    WarehouseComponent,
    AllowedContainerComponent,
    VesselScheduleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DataTableModule,
    MultiselectDropdownModule,
    AngularMultiSelectModule,
    MyDatePickerModule,
    MyDateRangePickerModule,
    Ng2MultiSelectDropDownModule.forRoot(),
    SortablejsModule.forRoot({ animation: 150 }),
    DataTablesModule,

      RouterModule.forRoot([
        {
          path: 'overview/loadPlanDetail',
          component: LoadPlanDetailComponent,
          children: [
            {path: 'optimized/:id', component: LpOptimisedComponent},
            { path: 'exception/:id', component: LpExceptionComponent , canDeactivate: [CanDeactivateGuard]},
            {path: 'carrierBooking/:id', component: LpCarrierBookingComponent}
          ]
        },
        {

        path: 'overview',
        component: OverviewComponent,

        children: [
         { path: 'exception', component: ExceptionComponent },
          {path: 'optimised', component: OptimisedComponent},
          {path: 'completed', component: CompletedComponent}
        ]
      },



        {
          path: 'orders',
          component: OrdersComponent
        },
        {
          path: 'schedules/new',
          component: ScheduleLoadPlanComponent
        },
        {
          path: 'schedules',
          component: SchedulesComponent
        },
        {
          path: 'decisionTrees/matchingRule',
          component: MatchingCriteriaComponent

        },

        {
          path: 'decisionTrees',
          component: DecisionTreesComponent
        },

        {
          path: 'configuration',
          component: ConfigurationComponent,

          children: [
            { path: 'location', component: LocationComponent },
            { path: 'destination', component: DestinationComponent },
            { path: 'origin', component: OriginComponent },
            { path: 'warehouse', component: WarehouseComponent },
            { path: 'containerFillRate', component: ContainerFillRateComponent },
            { path: 'allowedContainers', component: AllowedContainerComponent },
            { path: 'vesselSchedule', component: VesselScheduleComponent },
            { path: 'appData/containerType', component: ContainerTypeComponent },
            { path: 'appData/:type', component: ApplicationDataComponent }
          ]
        },
        {
          path: 'creatingRule/:mc',
          component: TemplateComponent

        }
      ])
  ],
  providers: [ServerService, GlobalService, CanDeactivateGuard, Origin, Destination,
    LoadPlanDetailFilterService,
    CountryDataService, OriginValues, DestinationValues, ExceptionAction, ExceptionActionValues, ExceptionComponent, OverviewService,
    NgLoadingSpinnerService, ExcelService, ApplicationDataService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true,
    }],
  bootstrap: [AppComponent]
})


export class AppModule { }
