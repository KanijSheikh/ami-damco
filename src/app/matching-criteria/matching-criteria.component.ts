import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ServerService} from '../server-service';
import {GlobalService} from "../global-service";
import {CountryDataService} from "../country-data-service";
import {IMyOptions} from "mydatepicker";
import {IMyDrpOptions} from "mydaterangepicker";
import {ApplicationDataService} from "../configuration/application-data.service";

@Component({
  selector: 'app-matching-criteria',
  templateUrl: './matching-criteria.component.html',
  styleUrls: ['./matching-criteria.component.css']
})
export class MatchingCriteriaComponent implements OnInit {

  decisionTreeName: string;
  treeOrder: number;

  sor: boolean;
  scgr: boolean;
  ruleName: string;
  colorof: string;
  instanceRule: rule;
  matchingCriteria: rule;
  orderRules: ruleGroup;
  cargoRules: ruleGroup;
  viewOrderCargoRules: ruleGroup;
  rightViewIsTrueAction: boolean;
  rightViewIsFalseAction: boolean;
  rightViewIsNotEmpty: boolean;
  rightViewIsMatchingCriteria: boolean;
  rightViewRule: rule;
  decisionTreeId: string;
  transports;
  serviceTypes;

  sampleDC: string;

  a: number[] = [44, 55];
  b: [number] = [66, 77];
  c: Array<number> = [88, 99];


  id: number = 0;

  operators: String[] = ['+', '-', '*', '/', '>', '>=', '<', '<=', '=', '!=', 'AND', 'OR', 'Contains', 'Between'];

  actionTypes: string [] = ['update', 'split', 'merge'];
  actionOperators: string [] = ['equals', 'begins with', 'ends with', 'contains', 'range', 'volume'];

  dataCatalogue: string[] = ['aaFlag', 'demandKey', 'poLineReference3', 'poLineReference4', 'poLineReference5', 'poLineReference6', 'poLineReference7', 'poLineReference8', 'poLineReference9', 'poLineReference10', 'poLineReference11', 'poLineReference12', 'poLineReference13', 'poLineReference14', 'poLineReference15', 'poLineReference16', 'poLineReference17', 'poLineReference18', 'poLineReference19', 'poLineReference20', 'actualRecieptDate', 'bookedCartons', 'bookedCbm', 'bookedQuantity', 'bookedWeight', 'consignee', 'expectedCargoReceiptDate', 'expectedCargoReceiptWeek', 'expectedDeliveryDate', 'latestDeliveryDate', 'latestCargoReceiptDate', 'orderType', 'podCountry', 'podProvince', 'podCity', 'porCountry', 'porProvince', 'porCity', 'plant', 'poNumber', 'poLine', 'productTypeCode', 'productType', 'pslv', 'receivedCatons', 'receivedCbm', 'receivedQuantity', 'receivedWeight', 'shipto', 'shipper', 'skuNumber', 'vendor', 'soReference1', 'soReference2', 'soReference3', 'soReference4', 'soReference5', 'soReference6', 'soReference7', 'soReference8', 'soReference9', 'soReference10', 'soReference11', 'soReference12', 'soReference13', 'soReference14', 'soReference15', 'soReference16', 'soReference17', 'soReference18', 'soReference19', 'soReference20', 'bookingNumber', 'carrier', 'destinationShipmentType', 'dischargePortCity', 'dischargePortCountry', 'dischargePortProvince', 'eta', 'etd', 'modeOfTransport', 'originServiceType', 'originShipmentType', 'shipmentStatus', 'currentDate', 'vol', 'wt', 'qty'];

  dataCatalogueWithCollectionAttributes: string[] = ['totalVolume', 'totalWeight', 'totalQuantity'];

  conditions: { id: number, dataCtl: string, opearator: string, text: string }[][] = [
    [{'id': this.id, 'dataCtl': 'vendor', 'opearator': '==', 'text': '0'}]
  ]

  cityColumns = ['podCity'];
  countryColumns = ['podCountry'];
  modeOfTransport = ['modeOfTransport'];
  originServiceType = ['originServiceType'];
  dateColumns = ['expectedCargoReceiptDate', 'expectedCargoReceiptWeek', 'expectedDeliveryDate', 'latestDeliveryDate', 'latestCargoReceiptDate'];

  cities: string[] = [];
  countries: string[] = [];
  destinationCityValues;
  dropdownSettings = {
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    itemsShowLimit: 2,
    allowSearchFilter: true
  };


  dropdownSettingsForAction = {
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    singleSelection: true,
    idField: 'id',
    textField: 'name',
    itemsShowLimit: 2,
    allowSearchFilter: true
  };

  dateRangePickerOptions: IMyDrpOptions = {
    dateFormat: 'dd-mm-yyyy',
    width: '100%'
  };

  public datePickerOptions: IMyOptions = {
    dateFormat: 'dd-mm-yyyy'
  };

  constructor(private router: Router, private serverService: ServerService, private globalService: GlobalService,
              private countryDataService: CountryDataService, private applicationDataService: ApplicationDataService) {
    this.instanceRule = new rule;
    let op1 = new operand();
    op1.value = 'Hello';
    op1.isConst = false;
    let op2 = new operand();
    op2.value = 'World';
    op2.isConst = false;
    let scond = new simpleCondition();
    scond.leftOperand = op1;
    scond.rightOperand = op2;
    scond.operator = 'AND';


    let cond = new condition();
    cond.arraySimpleConditions.push(scond);
    cond.arraySimpleConditions.push(scond);
    let op3 = new operand();
    op3.value = 'world';
    let scond2 = new simpleCondition();
    scond2.leftOperand = op3;
    cond.arraySimpleConditions.push(scond2);
    let tRule = new rule();
    tRule.arrayConditions.push(cond);
    tRule.name = 'First Rule';
    this.instanceRule.arrayConditions.push(cond);
    this.instanceRule.name = 'First Rule';
    this.sampleDC = '';
    this.rightViewIsNotEmpty = false;
    this.decisionTreeName = '';
    this.colorof = '#378dd1';
    this.sor = true;
    this.scgr = false;
    this.orderRules = new ruleGroup();
    this.cargoRules = new ruleGroup();
    this.matchingCriteria = new rule();
    this.initRule(this.matchingCriteria);
    /*this.orderRules.arrayRules.push(tRule);
    this.orderRules.arrayRules.push(tRule);
    this.orderRules.arrayRules.push(tRule);*/
    this.viewOrderCargoRules = this.orderRules;
    this.rightViewIsFalseAction = false;
    this.rightViewIsTrueAction = false;
    this.rightViewIsMatchingCriteria = false;
    this.decisionTreeId = '';


    const destinationValues = this.countryDataService.getDestinationValues();
    this.serverService.getDestination().subscribe((destination => {
      this.destinationCityValues = destination;
      for (const dest of this.destinationCityValues) {
        this.cities.push(dest.destinationName);
        this.cities.sort();
      }
    }));


    for (const dest of destinationValues) {
      if (dest.destinationId === 2) {
        this.countries.push(dest.name);
      }
    }

    this.applicationDataService.getModeOfTransports().subscribe(
      (response) => {
        this.transports = response;
        this.transports.push('ALL');
        this.transports.sort();
      });

    this.applicationDataService.getDamcoOriginServices().subscribe(
      (response) => {
        this.serviceTypes = response;
        this.serviceTypes.push('ALL');
        this.serviceTypes.sort();
      });
  }

  resetRightView() {
    this.rightViewIsMatchingCriteria = false;
    this.rightViewIsNotEmpty = false;
    this.rightViewIsFalseAction = false;
    this.rightViewIsTrueAction = false;
  }

  showMatchingCriteria() {
    this.rightViewRule = this.matchingCriteria;
    this.rightViewIsMatchingCriteria = true;
    this.rightViewIsNotEmpty = false;
    this.rightViewIsFalseAction = false;
    this.rightViewIsTrueAction = false;

  }

  showOrderRules() {
    this.sor = true;
    this.scgr = false;
    console.log('clicked on order rules');
    this.viewOrderCargoRules = this.orderRules;
    this.resetRightView();
  }

  showCargoGroupingRules() {
    this.sor = false;
    this.scgr = true;
    this.viewOrderCargoRules = this.cargoRules;
    console.log('clicked on cargo grouping rules');
    this.resetRightView();
  }

  addModifyNewRule() {
    let nRule: rule = new rule();
    this.initRule(nRule);
    if (this.sor == true) {
      this.orderRules.arrayRules.push(nRule);
    }
    if (this.scgr == true) {
      this.cargoRules.arrayRules.push(nRule);
    }
    this.resetRightView();
    this.showRightViewRule(nRule);
  }

  ngOnInit() {

    this.decisionTreeName = this.globalService.decisionTreeName;
    let dcs: string[] = this.dataCatalogue.sort();
    console.log(dcs);
    this.dataCatalogue = dcs;
    console.log('inside ngOnInit matching criteria, decisiontree id is ' + this.globalService.selectedDecisionTreeId);

    this.dataCatalogue = this.dataCatalogue.sort();
    this.operators = this.operators.sort();
    this.actionTypes = this.actionTypes.sort();
    if (this.globalService.selectedDecisionTreeId != '') {
      let resource = this.serverService.getRules();
      if (resource != null) {
        resource.subscribe(
          (response) => {
            console.log(response)
            const data = response;
            const count = Object.keys(data).length;
            console.log('data length is ' + count);
            let index: number = 0;
            for (; index < count; index++) {
              if (data[index].id == this.globalService.selectedDecisionTreeId)
                break;
            }
            if (index < count) {
              console.log('found the decision tree, now extracting');
              this.decisionTreeId = data[index].id;
              this.decisionTreeName = data[index].ruleName;
              this.treeOrder = data[index].treeOrder;
              console.log(data[index]);
              let pocoTree = data[index].poCoTree;
              console.log(pocoTree);
              let ordersTree = pocoTree.ctString;
              console.log(ordersTree);
              let collectiontree = pocoTree.collectionConditionTreeString;
              console.log(collectiontree);
              let ordersTreeJson = JSON.parse(ordersTree);
              let collectiontreeJson = JSON.parse(collectiontree);
              console.log(ordersTreeJson);
              console.log(collectiontreeJson);

              //START: matching criteria code
              let matchingCriteria = data[index].matchingCriteriaDto;
              console.log(matchingCriteria);
              let mindex: number = 0;
              let mcRule: rule = new rule();

              this.parseConditions(mcRule, matchingCriteria[mindex].criteriaValue);
              this.matchingCriteria = mcRule;
              console.log('parsed matching criteria==' + this.matchingCriteria);


              // END:  matching criteria code
              let itr = ordersTreeJson;
              while (itr.hasOwnProperty('trueChild') == true) {
                let pRule: rule = new rule();
                pRule.name = itr.ruleName;
                let expression = itr.conditionName;
                console.log('expression is ' + expression);
                this.parseConditions(pRule, expression);
                if (itr.hasOwnProperty('trueActions')) {
                  this.parseActions(pRule, itr.trueActions);
                  console.log('Action dto ==' + itr.trueActions);
                }
                if (itr.hasOwnProperty('falseActions')) {
                  this.parseFalseActions(pRule, itr.falseActions);
                  console.log(itr.falseActions);
                }
                this.orderRules.arrayRules.push(pRule);
                itr = itr.trueChild;
              }
              itr = collectiontreeJson;
              while (itr.hasOwnProperty('trueChild') == true) {
                let pRule: rule = new rule();
                let expression = itr.conditionName;
                pRule.name = itr.ruleName;
                console.log('expression is ' + expression);
                this.parseConditions(pRule, expression);
                if (itr.hasOwnProperty('trueActions')) {
                  this.parseActions(pRule, itr.trueActions);
                  console.log(itr.trueActions);
                }

                if (itr.hasOwnProperty('falseActions')) {
                  this.parseFalseActions(pRule, itr.falseActions);
                  console.log(itr.falseActions);
                }
                this.cargoRules.arrayRules.push(pRule);
                itr = itr.trueChild;
              }

            }
          },
          (error) => console.log(error)
        );
      }
    }
  }

  parseActions(pRule, trueActions) {
    if (trueActions.length > 0) {
      for (let trueAction of trueActions) {
        let ra = new ruleAction();

        if (trueAction.actionType === 'split' || trueAction.actionType === 'merge') {
          let ra = new ruleAction();
          ra.actionValue = trueAction.actionValue;
          ra.actionType = trueAction.actionType;
          ra.actionName = trueAction.actionName;
          ra.actionExpression = trueAction.actionExpression;
          if (trueAction.hasOwnProperty('actionRange') === true) {
            ra.actionValue = trueAction.actionRange + 'd';
          }
          ra.isActionAssigned = false;
          pRule.trueActions.push(ra);
        }

        if (trueAction.actionType === 'update' && (trueAction.actionValue === 'exception' || trueAction.actionValue === 'Exception')) {
          let ra = new ruleAction();
          ra.actionValue = trueAction.actionValue;
          ra.actionType = trueAction.actionType;
          ra.actionName = trueAction.actionName;
          ra.actionExpression = trueAction.actionExpression;
          for (let assignedAction of trueActions) {
            if (assignedAction.actionType === 'Assign') {
              ra.assignActionValue = assignedAction.actionValue;
              ra.assignActionType = assignedAction.actionType;
              ra.assignActionName = assignedAction.actionName;
              ra.actionValue = 'exception';
              ra.isActionAssigned = false;
            }
          }
          pRule.trueActions.push(ra);
        } else if (trueAction.actionType === 'update') {
          let ra = new ruleAction();
          ra.actionValue = trueAction.actionValue;
          ra.actionType = trueAction.actionType;
          ra.actionName = trueAction.actionName;
          ra.actionExpression = trueAction.actionExpression;
          ra.isActionAssigned = false;
          pRule.trueActions.push(ra);
        }
        console.log(pRule.trueActions);
      }
    }
  }

  parseFalseActions(pRule, falseActions) {
    if (falseActions.length > 0) {
      for (let falseAction of falseActions) {

        if (falseAction.actionType === 'split' || falseAction.actionType === 'merge') {
          let ra = new ruleAction();
          ra.actionValue = falseAction.actionValue;
          ra.actionType = falseAction.actionType;
          ra.actionName = falseAction.actionName;
          ra.actionExpression = falseAction.actionExpression;
          if (falseAction.hasOwnProperty('actionRange') === true) {
            ra.actionValue = falseAction.actionRange + 'd';
          }
          ra.isActionAssigned = false;
          pRule.falseActions.push(ra);
        }

        if (falseAction.actionType === 'update' && (falseAction.actionValue === 'exception' || falseAction.actionValue === 'Exception')) {
          let ra = new ruleAction();
          ra.actionValue = falseAction.actionValue;
          ra.actionType = falseAction.actionType;
          ra.actionName = falseAction.actionName;
          ra.actionExpression = falseAction.actionExpression;
          for (let assignedAction of falseActions) {
            if (assignedAction.actionType === 'Assign') {
              ra.assignActionValue = assignedAction.actionValue;
              ra.assignActionType = assignedAction.actionType;
              ra.assignActionName = assignedAction.actionName;
              ra.actionValue = 'exception';
              ra.isActionAssigned = false;
            }
          }
          pRule.falseActions.push(ra);
        } else if (falseAction.actionType === 'update') {
          let ra = new ruleAction();
          ra.actionValue = falseAction.actionValue;
          ra.actionType = falseAction.actionType;
          ra.actionName = falseAction.actionName;
          ra.actionExpression = falseAction.actionExpression;
          ra.isActionAssigned = false;
          pRule.falseActions.push(ra);
        }

        //
        // if (falseAction.actionType === 'update' && (falseAction.actionValue === 'release' || falseAction.actionValue === 'Release')) {
        //   let ra = new ruleAction();
        //   ra.actionValue = falseAction.actionValue;
        //   ra.actionType = falseAction.actionType;
        //   ra.actionName = falseAction.actionName;
        //   ra.actionExpression = falseAction.actionExpression;
        //   ra.isActionAssigned = false;
        //   pRule.falseActions.push(ra);
        // }
        console.log(pRule.falseActions);
      }
    }
  }

  parseConditions(pRule: rule, expression: string) {
    const tokens = expression.split(' ');
    console.log('tokens');
    console.log(tokens);
    let currentTokenIndex = 0;
    const currentToken = tokens[currentTokenIndex];
    while (currentTokenIndex < tokens.length && tokens[currentTokenIndex] === '(') {
      const con = new condition();
      currentTokenIndex++;
      while (currentTokenIndex < tokens.length && tokens[currentTokenIndex] === '(') {
        let leftCol = '';
        const scon = new simpleCondition();
        currentTokenIndex++;
        if (currentTokenIndex < tokens.length) {
          scon.leftOperand = new operand();
          scon.leftOperand.value = tokens[currentTokenIndex];
          leftCol = scon.leftOperand.value;
          currentTokenIndex++;
          if (currentTokenIndex < tokens.length) {
            scon.operator = tokens[currentTokenIndex];
            currentTokenIndex++;
            if (currentTokenIndex < tokens.length) {
              scon.rightOperand = new operand();
              if (this.isCountryField(leftCol) || this.isCityField(leftCol)) {
                scon.rightOperand.multiValue = decodeURI(tokens[currentTokenIndex]).split(',');
              } else if (this.isDateField(leftCol)) {
                this.parseDateValues(tokens[currentTokenIndex], scon.rightOperand, scon.operator);
              } else {
                scon.rightOperand.value = tokens[currentTokenIndex];
              }
              currentTokenIndex++;
              if (currentTokenIndex < tokens.length && tokens[currentTokenIndex] === ')') {
                con.arraySimpleConditions.push(scon);
                currentTokenIndex++;
                if (currentTokenIndex < tokens.length && tokens[currentTokenIndex] !== ')') {
                  con.operators.push(this.getConditionalOperator(tokens[currentTokenIndex]));
                  currentTokenIndex++;
                }
              }
            }
          }
        }
      }
      if (currentTokenIndex < tokens.length && tokens[currentTokenIndex] === ')') {
        pRule.arrayConditions.push(con);
        currentTokenIndex++;
        if (currentTokenIndex < tokens.length && tokens[currentTokenIndex] !== ')') {
          pRule.operators.push(this.getConditionalOperator(tokens[currentTokenIndex]));
          currentTokenIndex++;
        }
      }
    }
  }


  parseDateValues(value, rightOperand, operator) {
    if (operator === 'Between') {
      const split = value.split('%20');
      rightOperand.dateRange = {
        beginDate: this.getParsedDate(split[0]),
        endDate: this.getParsedDate(split[1]),
        formatted: value.replace('%20', ' - ')
      };
    } else {
      rightOperand.date = {date: this.getParsedDate(value), formatted: value};
    }
  }

  getParsedDate(date) {
    var split = date.split('-');
    return {year: +split[2], month: +split[1], day: +split[0]};
  }

  onSave() {
    console.log(this.getConditionString());
    this.router.navigate(['/creatingRule', this.getConditionString()]);
  }

  getConditionString() {
    var condString = '"matchingCriteriaDto": [ ';
    for (let condition of this.conditions) {
      for (let child of condition) {
        condString = condString.concat(' { ');
        condString = condString.concat(this.getString(child));
        condString = condString.concat('},');
      }


    }
    condString = condString.replace(/.$/, ']');
    return condString;
  }

  getString(condition: { dataCtl: string, opearator: string, text: string }) {
    return condition ? '"criteriaName" : "' + condition.dataCtl.concat('","criteriaValue" : "').concat(condition.text).concat('"') : null;
  }

  addFalseAction() {
    this.rightViewRule.falseActions.push(this.getNewAction());
  }


  addTrueAction() {
    this.rightViewRule.trueActions.push(this.getNewAction());
  }

  getNewAction() {
    let ra = new ruleAction();
    ra.actionType = '';
    ra.actionName = '';
    ra.actionValue = '';
    ra.actionExpression = '';
    ra.assignActionType = 'Assign';
    ra.assignActionName = 'Exception';
    ra.assignActionValue = 'Customer'
    return ra;
  }

  showRightViewRule(pRule) {
    this.resetRightView();
    this.rightViewRule = pRule;
    this.rightViewIsNotEmpty = true;

  }

  deleteFalseAction(act: ruleAction) {
    let index = this.rightViewRule.falseActions.indexOf(act);
    this.rightViewRule.falseActions.splice(index, 1);
  }


  deleteTrueAction(act: ruleAction) {
    let index = this.rightViewRule.trueActions.indexOf(act);
    this.rightViewRule.trueActions.splice(index, 1);
  }

  deleteSimpleCondition(scon: simpleCondition, pCon: condition) {

    let index = pCon.arraySimpleConditions.indexOf(scon);
    pCon.arraySimpleConditions.splice(index, 1);
    if (pCon.arraySimpleConditions.length == 0) {
      let cindex = this.rightViewRule.arrayConditions.indexOf(pCon);
      this.rightViewRule.arrayConditions.splice(cindex, 1);
    }
    if (this.rightViewRule.arrayConditions.length == 0) {

      let rindex = this.viewOrderCargoRules.arrayRules.indexOf(this.rightViewRule);
      this.viewOrderCargoRules.arrayRules.splice(rindex, 1);
      this.resetRightView();
    }
  }

  addLevelOneCondition(pRule: rule) {
    let cond = new condition();
    this.addSimpleCondition(cond);
    pRule.arrayConditions.push(cond);
  }

  addSimpleCondition(pCond: condition) {
    let scond = new simpleCondition();
    scond.leftOperand = new operand();
    scond.rightOperand = new operand();
    scond.leftOperand.value = '';
    scond.rightOperand.value = '';
    scond.operator = '';
    pCond.arraySimpleConditions.push(scond);
    console.log('adding simple condition to condition');
  }

  addModifyTrueActions(pRule: rule) {
    this.resetRightView();
    this.rightViewRule = pRule;
    this.rightViewIsTrueAction = true;

  }

  addModifyFalseActions(pRule: rule) {
    this.resetRightView();
    this.rightViewRule = pRule;
    this.rightViewIsFalseAction = true;

  }

  initRule(nRule: rule) {


    let scond: simpleCondition = new simpleCondition();
    scond.leftOperand = new operand();
    scond.rightOperand = new operand();
    scond.leftOperand.value = '';
    scond.rightOperand.value = '';
    scond.operator = '';
    let cond: condition = new condition();
    cond.arraySimpleConditions.push(scond);
    nRule.arrayConditions.push(cond);

  }

  returnRuleName() {
    let rrn: string = '';
    if (this.decisionTreeId === '') {
    }
    else {
      rrn = "\"id\"" + " : " + "\"" + this.decisionTreeId + "\", ";
      rrn = rrn + "\"treeOrder\"" + " : " + "\"" + this.treeOrder + "\", ";
    }
    rrn = rrn + "\"ruleName\"" + " : " + "\"" + this.decisionTreeName + "\"";

    return rrn;
  }

  returnMatchingCriteria() {
    let rmc: string = '';
    rmc = rmc + "\"matchingCriteriaDto\":[";
    let i: number;

    //  rmc = rmc + "{\"criteriaName\" : " + "\"" + sc.leftOperand.value + "\", \"criteriaValue\" : \"" + sc.rightOperand.value + "\"}";
    rmc = rmc + "{\"criteriaName\" : " + "\"" + 'Matching Criteria' + "\", \"criteriaValue\" : \"";
    let conIndex: number = 0;

    for (let pCon of this.matchingCriteria.arrayConditions) {
      if (conIndex > 0) {
        rmc = rmc + " " + this.returnEncodeOperator(this.matchingCriteria.operators[conIndex - 1]) + " ";
      }
      rmc = rmc + "(";
      let sconIndex = 0;
      for (let pSCon of pCon.arraySimpleConditions) {
        rmc = rmc + " ( " + pSCon.leftOperand.value + " " + this.returnEncodeOperator(pSCon.operator) + " " + this.getRightOperandValue(pSCon.leftOperand.value, pSCon) + " )";
        if ((sconIndex + 1) != pCon.arraySimpleConditions.length) {
          rmc = rmc + " " + this.returnEncodeOperator(pCon.operators[sconIndex]);
        }
        sconIndex = sconIndex + 1;
      }
      rmc = rmc + " )";

      conIndex = conIndex + 1;
    }
    rmc = rmc + '"} ]';


    console.log('Matching Criteria=====' + rmc);

    return rmc;
  }

  getConditionalOperator(op: string) {
    if (op == "&") {
      return "AND";
    } else if (op == "|") {
      return "OR";
    }
    return op;
  }


  returnEncodeOperator(op: string) {
    if (op == "AND") {
      return "&";
    } else if (op == "OR") {
      return "|";
    }
    return op;
  }

  returnEncodeRuleConditionAction(pRule: rule) {
    if (pRule.arrayConditions.length == 0) {
      return "";
    }
    let pr: string = "";

    pr = "\\\"ruleName\\\" : \\\"" + pRule.name + "\\\",";
    pr = pr + "\\\"conditionName\\\" : \\\"";

    let conIndex: number = 0;
    for (let pCon of pRule.arrayConditions) {
      if (conIndex > 0) {
        pr = pr + " " + this.returnEncodeOperator(pRule.operators[conIndex - 1]) + " ";
      }
      pr = pr + "(";
      let sconIndex = 0;
      for (let pSCon of pCon.arraySimpleConditions) {
        pr = pr + " ( " + pSCon.leftOperand.value + " " + this.returnEncodeOperator(pSCon.operator) + " " + this.getRightOperandValue(pSCon.leftOperand.value, pSCon) + " )";
        if ((sconIndex + 1) != pCon.arraySimpleConditions.length) {
          pr = pr + " " + this.returnEncodeOperator(pCon.operators[sconIndex]);
        }
        sconIndex = sconIndex + 1;
      }
      pr = pr + " )";

      conIndex = conIndex + 1;
    }
    pr = pr + "\\\"";
    if (pRule.trueActions.length > 0) {

      let actionRange: string = "";
      pr = pr + "," + "\\\"trueActions\\\" : ["

      for (let trueAction of pRule.trueActions) {

        let actionValue: string = trueAction.actionValue;
        let exp: string = '';
        if (trueAction.actionType == 'split') {
          if (actionValue !== '') {

            if ((actionValue.charAt(actionValue.length - 1) == 'd') ||
              (actionValue.charAt(actionValue.length - 1) == 'D')) {
              actionRange = actionValue.substr(0, actionValue.length - 1);
            }
          }
          exp = trueAction.actionExpression;
        } else {
          exp = '';
        }
        pr = pr + "{" + "\\\"actionName\\\" : \\\"" + trueAction.actionName + "\\\",";
        pr = pr + "\\\"actionType\\\" : \\\"" + trueAction.actionType + "\\\", ";
        pr = pr + "\\\"actionExpression\\\" : \\\"" + exp + "\\\", ";

        if (actionRange != "") {
          pr = pr + "\\\"actionRange\\\" : \\\"" + actionRange + "\\\",";
          pr = pr + "\\\"actionValue\\\" : \\\"" + "\\\"},";
        } else {
          pr = pr + "\\\"actionValue\\\" : \\\"" + trueAction.actionValue + "\\\"},";
        }

        if (trueAction.actionType == 'update' && (trueAction.actionValue == 'exception' || trueAction.actionValue == 'Exception')) {
          pr = pr + "{" + "\\\"actionName\\\" : \\\"" + trueAction.assignActionName + "\\\",";
          pr = pr + "\\\"actionType\\\" : \\\"" + trueAction.assignActionType + "\\\", ";
          pr = pr + "\\\"actionValue\\\" : \\\"" + trueAction.assignActionValue + "\\\"},";
        }
      }
      pr = pr + "]";
      pr = pr.replace(',]', ']');
      console.log(pr);
    }
    if (pRule.falseActions.length > 0) {
      let actionRange: string = "";
      pr = pr + "," + "\\\"falseActions\\\" : ["

      for (let falseAction of pRule.falseActions) {
        let actionValue: string = falseAction.actionValue;
        let exp: string = '';
        if (falseAction.actionType == 'split') {
          if (actionValue !== '') {

            if ((actionValue.charAt(actionValue.length - 1) == 'd') ||
              (actionValue.charAt(actionValue.length - 1) == 'D')) {
              actionRange = actionValue.substr(0, actionValue.length - 1);
            }
          }
          exp = falseAction.actionExpression;
        } else {
          exp = '';
        }
        pr = pr + "{" + "\\\"actionName\\\" : \\\"" + falseAction.actionName + "\\\",";
        pr = pr + "\\\"actionType\\\" : \\\"" + falseAction.actionType + "\\\", ";
        pr = pr + "\\\"actionExpression\\\" : \\\"" + exp + "\\\", ";

        if (actionRange != "") {
          pr = pr + "\\\"actionRange\\\" : \\\"" + actionRange + "\\\",";
          pr = pr + "\\\"actionValue\\\" : \\\"" + "\\\"},";
        } else {
          pr = pr + "\\\"actionValue\\\" : \\\"" + falseAction.actionValue + "\\\"},";
        }

        if (falseAction.actionType == 'update' && (falseAction.actionValue == 'exception' || falseAction.actionValue == 'Exception')) {
          pr = pr + "{" + "\\\"actionName\\\" : \\\"" + falseAction.assignActionName + "\\\",";
          pr = pr + "\\\"actionType\\\" : \\\"" + falseAction.assignActionType + "\\\", ";
          pr = pr + "\\\"actionValue\\\" : \\\"" + falseAction.assignActionValue + "\\\"},";
        }
      }
      pr = pr + "]";
      pr = pr.replace(',]', ']');
    }

    pr = pr + ", \\\"falseChild\\\" : {}";
    console.log(pr);
    return pr;
  }

  getRightOperandValue(colname, cond) {
    if (this.isCityField(colname) || this.isCountryField(colname)) {
      return encodeURI(cond.rightOperand.multiValue.join());
    } else if (this.isDateField(colname)) {
      if (cond.operator === 'Between') {
        return cond.rightOperand.dateRange.formatted.replace(' - ', '%20');
      } else {
        return cond.rightOperand.date.formatted;
      }
    }
    return cond.rightOperand.value;
  }


  returnCargoTree() {
    let rot: string = "";
    rot = "\"collectionConditionTreeString\" : \"{";

    for (let pRule of this.cargoRules.arrayRules) {
      rot = rot + this.returnEncodeRuleConditionAction(pRule);
      rot = rot + ", \\\"trueChild\\\" : {";
    }
    let rindex: number = 0;
    for (let pRule of this.cargoRules.arrayRules) {
      rot = rot + " } ";
      rindex = rindex + 1;
    }
    rot = rot + " }\" ";
    return rot;
  }

  returnOrdersTree() {
    let rot: string = "";
    rot = "\"ctString\" : \"{";

    for (let pRule of this.orderRules.arrayRules) {
      rot = rot + this.returnEncodeRuleConditionAction(pRule);
      rot = rot + ", \\\"trueChild\\\" : {";
    }
    let rindex: number = 0;
    for (let pRule of this.orderRules.arrayRules) {
      rot = rot + " } ";
      rindex = rindex + 1;
    }
    rot = rot + " }\" ";
    return rot;
  }

  returnTrees() {
    let rt: string = "";
    rt = "\"poCoTree\" : {";
    rt = rt + this.returnOrdersTree();
    rt = rt + ", " + this.returnCargoTree();
    rt = rt + "}";
    return rt;
  }

  createDecisionTreeJson() {
    let dtj: string = '{';
    dtj = dtj + this.returnRuleName();
    dtj = dtj + ', ' + this.returnMatchingCriteria();
    dtj = dtj + ', ' + this.returnTrees();
    dtj = dtj + '}';
    console.log('Decesion Tree ====' + dtj);
    this.serverService.storeRules(dtj).subscribe(
      (response) => {
        if (response.headers.get('lastid') != null) {
          this.decisionTreeId = response.headers.get('lastid');
          console.log('decision tree id is ' + this.decisionTreeId);
          this.router.navigate(['/decisionTrees']);
        }
      }
    );

  }

  isCityField(colName) {
    return this.cityColumns.includes(colName);
  }

  isCountryField(colName) {
    return this.countryColumns.includes(colName);
  }

  isDateField(colName) {
    return this.dateColumns.includes(colName);
  }

  isSpecialField(colName) {
    return this.isCityField(colName) ||
      this.isCountryField(colName) ||
      this.isDateField(colName);
  }

  isSpecialActionField(colName) {
    return this.isModeOfTransportField(colName) ||
      this.isOriginServiceTypeField(colName);
  }

  isModeOfTransportField(colName) {
    return this.modeOfTransport.includes(colName);
  }

  isOriginServiceTypeField(colName) {
    return this.originServiceType.includes(colName);
  }
}

class operand {
  isConst: boolean;
  value: string;
  multiValue: string[];
  dateRange: {} = {};
  date;
}

class simpleCondition {
  rightOperand: operand;
  leftOperand: operand;
  operator: string;

}

class ruleAction {
  actionType: string;
  actionName: string;
  actionValue: string;
  actionExpression: string;
  assignActionType: string;
  assignActionName: string;
  assignActionValue: string;
  isActionAssigned: boolean;
}


class condition {
  arraySimpleConditions: simpleCondition[] = new Array();
  operators: string[] = new Array();
}

class rule {
  name: string;
  arrayConditions: condition[] = new Array();
  operators: string[] = new Array();
  trueActions: ruleAction [] = new Array();
  falseActions: ruleAction [] = new Array();
}

class ruleGroup {
  arrayRules: rule [] = new Array();
}

