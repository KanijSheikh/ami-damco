import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchingCriteriaComponent } from './matching-criteria.component';

describe('MatchingCriteriaComponent', () => {
  let component: MatchingCriteriaComponent;
  let fixture: ComponentFixture<MatchingCriteriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchingCriteriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchingCriteriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
