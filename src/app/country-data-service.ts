import {Injectable} from '@angular/core';
import {Origin} from './overview/origin';
import {OriginValues} from './overview/origin-values';
import {Destination} from './overview/destination';
import {DestinationValues} from './overview/destination-values';
import {ExceptionAction} from "./overview/exception-action";
import {ExceptionActionValues} from "./overview/exception-action-values";

@Injectable()
export class CountryDataService {

  getOrigin() {
    return [
      new Origin(1, 'City' ),
      new Origin(2, 'Country' ),
      new Origin(3, 'Region' ),
    ];
  }

  getOriginValues() {
    return [
      new OriginValues(1, 1, 'Phnom Penh'),
      new OriginValues(2, 1, 'Yantian'),
      new OriginValues(3, 1, 'Jakarta'),
      new OriginValues(4, 1, 'Chennai'),
      new OriginValues(5, 1, 'Istanbul'),
      new OriginValues(6, 1, 'Pakistan'),
      new OriginValues(7, 1, 'Ho Chi Minh'),

      new OriginValues(7, 2, 'Camododia'),
      new OriginValues(8, 2, 'China'),
      new OriginValues(9, 2, 'Vietnam'),
      new OriginValues(10, 2, 'Indonesia'),
      new OriginValues(11, 2, 'India'),
      new OriginValues(12, 2, 'Sri Lanka'),
      new OriginValues(13, 2, 'Turkey'),

      new OriginValues(14, 3, 'Asia'),
      new OriginValues(15, 3, 'Imea'),
      new OriginValues(16, 3, 'Europe'),
      new OriginValues(17, 3, 'Americas'),

    ];
  }

  getDestination() {
    return [
      new Destination(1, 'City' ),
      new Destination(2, 'Country' ),
      new Destination(3, 'Region' ),
    ];
  }

  getDestinationValues() {
    return [
      new DestinationValues(1, 1, 'Shanghai'),
      new DestinationValues(2, 1, 'Busan'),
      new DestinationValues(3, 1, 'Tokyo'),
      new DestinationValues(4, 1, 'Memphis'),
      new DestinationValues(5, 1, 'Heijen'),
      new DestinationValues(6, 1, 'Laakdal'),
      new DestinationValues(7, 1, 'Mansfield'),

      new DestinationValues(8, 2, 'China'),
      new DestinationValues(9, 2, 'Korea'),
      new DestinationValues(10, 2, 'Japan'),
      new DestinationValues(11, 2, 'USA'),
      new DestinationValues(12, 2, 'Netherlands'),
      new DestinationValues(13, 2, 'Belgium'),
      new DestinationValues(14, 2, 'Germany'),
      new DestinationValues(15, 2, 'France'),
      new DestinationValues(16, 2, 'United Kingdom'),

      new DestinationValues(17, 3, 'Greater China'),
      new DestinationValues(18, 3, 'Apla'),
      new DestinationValues(19, 3, 'Emea'),
      new DestinationValues(20, 3, 'North America'),

    ];
  }
}
