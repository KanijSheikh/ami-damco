import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { DataTable, DataTableTranslations, DataTableResource } from 'angular-4-data-table-fix';
import { Router } from '@angular/router';
import {ServerService} from '../../server-service';
import {GlobalService} from '../../global-service';
import {OverviewService} from '../overview.service';
import {Subscription} from 'rxjs/Subscription';


@Component({
  selector: 'app-exception',
  templateUrl: './exception.component.html',
  styleUrls: ['./exception.component.css']
})
export class ExceptionComponent implements OnInit, OnDestroy {

  exceptionLoadPlan;
  loadPlanTableResource = new DataTableResource(this.exceptionLoadPlan);
  lpCount = 0;
  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };

  @ViewChild(DataTable) exceptionTable;
  subscription: Subscription;

  filterVal = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: '' };

  constructor(private route: Router, private serverService: ServerService, private globalService: GlobalService,
              private overviewService: OverviewService) {

    this.subscription = this.overviewService.getFilterData().subscribe(val => {
      this.filterVal = val;
      this.initialiseTable(val);
    });
  }

  private initialiseTable(val) {
    this.loadPlanTableResource.query({}).then(orders => this.lpCount = orders.filter(this.filter(val, this)).length);
    this.reloadLoadPlan({'sortBy': 'cutOff', 'offset': '0', 'limit': 10});
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }



  ngOnInit() {

    this.overviewService.dataChanged
      .subscribe(
        () => {
          this.getData();
        }
      );
    this.getData();
  }

  private getData() {
    this.serverService.getExceptionLoadPlan().subscribe(
      (loadPlans) => {
        this.exceptionLoadPlan = loadPlans;
        this.exceptionLoadPlan = [...this.exceptionLoadPlan];
        this.loadPlanTableResource = new DataTableResource(this.exceptionLoadPlan);
        this.filterVal = this.overviewService.getLatestFilterValue();
        if (this.isFilterApplied()) {
          this.loadPlanTableResource.query({}).then(orders => this.lpCount = orders.filter(this.filter(this.filterVal, this)).length);
        }
        this.reloadLoadPlan({'sortBy': 'cutOff', 'offset': '0', 'limit': '10'});
        return this.exceptionLoadPlan;
      }
    );
  }

  reloadLoadPlan(params) {
    if (this.isFilterApplied()) {
      const val = this.filterVal;
      this.loadPlanTableResource.query(params, this.filter(val, this) ).then(orders => {
        this.exceptionLoadPlan = orders;
      });

    } else {

      this.loadPlanTableResource.query(params).then(exceptionLoadPlan => this.exceptionLoadPlan = exceptionLoadPlan);
      this.loadPlanTableResource.count().then(count => this.lpCount = count);
    }
  }


  isFilterApplied() {
    return this.filterVal.origin !== '' ||
      this.filterVal.carrier !== '' ||
      this.filterVal.cutOffDate !== '' ||
      this.filterVal.mot !== '' ||
      this.filterVal.lpNumber !== '' ||
      this.filterVal.destination !== '';
  }

  filter(val, thisArg) {

    return function (element) {
      return (val.carrier === '' || thisArg.carrierMatch(element, val)) &&
        (val.origin === '' || thisArg.originMatch(element, val)) &&
        (val.destination === '' || thisArg.destinationMatch(element, val)) &&
        (val.cutOffDate === '' || val.cutOffDate.formatted === element.cutOff ) &&
        (val.mot === '' ||  element.modeOfTransport.toUpperCase().indexOf(val.mot.toUpperCase() )  > -1) &&
        (val.lpNumber === '' || element.loadPlanNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1
          || thisArg.quickSearch(element, val));
    };
  }

  quickSearch(element, val) {
    for (const container of element.containerDtos) {
      if (container.containerNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1) {
        return true;
      }else {
        for (const po of container.purchaseOrderOp) {
          if (po.poNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1 ||
              po.soNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1) {
            return true;
          }
        }
      }
    }
    return false;
  }

  carrierMatch(element, val) {
    for (const container of element.containerDtos) {
      if (container.carrier && container.carrier.toUpperCase().indexOf(val.carrier.toUpperCase()) > -1) {
        return true;
      }
    }
    return false;
  }

  destinationMatch(element, val) {
    for (const container of element.containerDtos) {
      if (container.purchaseOrderOp && container.purchaseOrderOp[0].podCity &&
        container.purchaseOrderOp[0].podCity.toUpperCase().indexOf(val.destination.toUpperCase()) > -1) {
        return true;
      }
    }
    return false;
  }

  originMatch(element, val) {
    for (const container of element.containerDtos) {
      if (container.purchaseOrderOp && container.purchaseOrderOp[0].loadPortCity &&
        container.purchaseOrderOp[0].loadPortCity.toUpperCase().indexOf(val.origin.toUpperCase()) > -1) {
        return true;
      }
    }
    return false;
  }

  cellColor(car) {
    return 'rgb(255, 255,' + (155 + Math.floor(100 - ((car.rating - 8.7) / 1.3) * 100)) + ')';
  }

  rowClick(rowEvent) {
    this.globalService.isExceptionActive = true;
    this.globalService.isOptimisedActive = false;
    this.globalService.isCarrierActive = false;

   this.route.navigate(['overview/loadPlanDetail/exception', rowEvent.row.item.id]);
   this.globalService.id = rowEvent.row.item.id;
   this.globalService.LpNumber = rowEvent.row.item.loadPlanNumber;
  }
}
