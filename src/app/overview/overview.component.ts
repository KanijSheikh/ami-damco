import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Origin} from './origin';
import {Destination} from './destination';
import {CountryDataService} from '../country-data-service';
import {ServerService} from '../server-service';
import {GlobalService} from '../global-service';
import {IMyOptions} from 'mydatepicker';
import {OverviewService} from './overview.service';
import {ApplicationDataService} from '../configuration/application-data.service';


@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'yyyy-mm-dd',
  };

  showFilter = false;
  selectedOrigin: Origin = new Origin(1, 'City');
  origins: Origin[];
  selectedDate;
  originValues;

  status: string;


  selectedDestination: Destination = new Destination(1, 'City');
  destinations: Destination[];
  destinationValues;
  id = 0;
  selectedModeOfTransport = 'ALL';
  selectedOriginValues;
  selectedDestinationValues;


  transports;

  selectedServiceType = 'ALL';
  serviceTypes;


  filterVal = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: ''};


  originCities: string[] = [];
  destinationCities: string[] = [];

  dropdownSettings = {
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    singleSelection: false,
    itemsShowLimit: 2,
    allowSearchFilter: true
  };

  dropdownSettingsForOrigin = {
    singleSelection: true,
    itemsShowLimit: 2,
    allowSearchFilter: true
  };

  constructor(private router: Router, private countryDataService: CountryDataService, private serverService: ServerService,
              private overviewService: OverviewService, private globalService: GlobalService,
              private applicationDataService: ApplicationDataService) {
    this.origins = this.countryDataService.getOrigin();
    this.destinations = this.countryDataService.getDestination();

    this.serverService.getOrigin().subscribe(
      (origins) => {
        this.originValues = origins;
        for (let origin of this.originValues) {
          if (!(this.originCities.includes(origin.originName))) {
            this.originCities.push(origin.originName);
            this.originCities.sort();
            this.originCities = [...this.originCities];
          }
        }
      }
    );

    this.serverService.getDestination().subscribe(
      (destinations) => {
        this.destinationValues = destinations;
        for (let destination of this.destinationValues) {
          if (!(this.destinationCities.includes(destination.destinationName))) {
            this.destinationCities.push(destination.destinationName);
            this.destinationCities.sort();
            this.destinationCities = [...this.destinationCities];
          }

        }
      }
    );

    this.applicationDataService.getModeOfTransports().subscribe(
      (response) => {
        this.transports = response;
        this.transports.push('ALL');
        this.transports.sort();
      });
    this.applicationDataService.getDamcoOriginServices().subscribe(
      (response) => {
        this.serviceTypes = response;
        this.serviceTypes.push('ALL');
        this.serviceTypes.sort();
      });
  }

  ngOnInit() {
    this.filterVal = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: ''};
    this.overviewService.clearLatestFilterValue();
    this.router.navigate(['/overview/exception']);
  }

  onClear() {
    this.filterVal = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: ''};
    this.overviewService.sendFilterData(this.filterVal);
  }

  onOriginSelect(originid) {
    this.selectedOriginValues = [];
    if (originid === '1') {
      this.originCities = [...this.originCities];
    } else if (originid === '2') {
      this.originCities = ['Vietnam', 'China', 'Camododia', 'Turkey', 'Sri Lanka', 'India', 'Indonesia'];
    } else if (originid === '3') {
      this.originCities = ['Asia', 'Imea', 'Europe', 'Americas'];
    }
  }

  onDestinationSelect(destinationId) {
    this.selectedDestinationValues = [];
    if (destinationId === '1') {
      this.destinationCities = [...this.destinationCities];
    } else if (destinationId === '2') {
      this.destinationCities = ['China', 'Korea', 'Japan', 'USA', 'Netherlands', 'Belgium', 'Germany', 'France', 'United Kingdom'];
    } else if (destinationId === '3') {
      this.destinationCities = ['Greater China', 'Apla', 'Emea', 'North America'];
    }
  }


  createLoadPlan(event) {
    this.overviewService.onDataChange();

    const originNames = [];
    for (let finalOriginValue of this.selectedOriginValues) {
      originNames.push('"' + finalOriginValue + '"');

    }

    const destinationNames = [];
    for (let finalDestValue of this.selectedDestinationValues) {
      destinationNames.push('"' + finalDestValue + '"');
    }

    this.origins = this.countryDataService.getOrigin().filter((item) => item.id == this.selectedOrigin.id);
    this.destinations = this.countryDataService.getDestination().filter((item) => item.id == this.selectedDestination.id);


    const body = '{ "serviceType" : ' + '"' + this.selectedServiceType + '"' + ','
      + '"loadPlanNumber" : ' + '"Lp-100"' + ','
      + '"loadPlanName" : ' + '"LPNAME-1"' + ','
      + '"modeOfTransport" : ' + '"' + this.selectedModeOfTransport + '"' + ','
      + '"cutOff" : ' + '"' + this.selectedDate.formatted + '"' + ','
      + '"originDto" : {' + '"' + 'type' + '" : ' + '"' + this.origins[0].name + '"' + ',' +
      '"originValue": [ ' + originNames + ' ] } ,'
      + '"destinationDto" : {' + '"' + 'type' + '" : ' + '"' + this.destinations[0].name + '"' + ',' +
      '"destinationValue": [ ' + destinationNames + ' ] }' + '}';

    console.log('body==='+body);

    this.serverService.createLoadPlan(body).subscribe(
      (response) => {
        if (response.headers.get('status') === 'exception') {
          this.overviewService.onDataChange();
        } else {
          this.router.navigate(['/overview/optimised']);
          this.globalService.isLpOptimisedActive = true;
          this.globalService.isLpExceptionActive = false;
          this.globalService.isLpCompleted = false;
        }
      }
    );

  }

  onOriginChange() {
  }

  onDestinationChange() {

  }

  redirectOptimized() {
    this.globalService.isLpOptimisedActive = true;
    this.globalService.isLpExceptionActive = false;
    this.globalService.isLpCompleted = false
    this.router.navigate(['overview/optimised/']);
  }

  redirectCompleted() {
    this.globalService.isLpCompleted = true
    this.globalService.isLpOptimisedActive = false;
    this.globalService.isLpExceptionActive = false;
    this.router.navigate(['overview/completed']);
  }

  redirectException() {
    this.globalService.isLpExceptionActive = true;
    this.globalService.isLpCompleted = false
    this.globalService.isLpOptimisedActive = false;
    this.router.navigate(['overview/exception']);
  }

  filterContent() {
    const val = this.filterVal;
    if (!val.cutOffDate) {
      val.cutOffDate = '';
    }
    this.overviewService.sendFilterData(val);
  }

}

