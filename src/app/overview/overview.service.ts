
import { EventEmitter } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

export class OverviewService {
  dataChanged = new EventEmitter<boolean>();
  addContainer = new EventEmitter<boolean>();

  filterValueSubject = new Subject<{}>();
  filterValue;

  onDataChange() {
    this.dataChanged.emit(true);
  }

  onAddingContainer() {
    this.addContainer.emit(true);
  }

  sendFilterData(value: {}) {
    this.filterValue = value;
    this.filterValueSubject.next(value);
  }

  getFilterData(): Observable<any> {
    return this.filterValueSubject.asObservable();
  }

  getLatestFilterValue() {
    return this.filterValue;
  }

 clearLatestFilterValue() {
     this.filterValue = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: '' };
  }

}
