import {Injectable} from '@angular/core';

@Injectable()
export class ExceptionActionValues {
  constructor(public id: number, public actionId: number , public name: string) { }
}
