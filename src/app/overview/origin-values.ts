import {Injectable} from '@angular/core';

@Injectable()
export class OriginValues {
  constructor(public id: number, public originid: number , public name: string) { }
}
