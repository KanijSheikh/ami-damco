import {Injectable} from '@angular/core';

@Injectable()
export class ContainerSize {
  constructor(public id: number, public name: string) { }
}


@Injectable()
export class DischargePort {
  constructor(public id: number, public name: string) { }
}

@Injectable()
export class Carrier {
  constructor(public id: number, public name: string) { }
}


@Injectable()
export class SplitOption {
  constructor(public id: number, public name: string) { }
}
