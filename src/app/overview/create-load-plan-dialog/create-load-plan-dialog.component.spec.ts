import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLoadPlanDialogComponent } from './create-load-plan-dialog.component';

describe('CreateLoadPlanDialogComponent', () => {
  let component: CreateLoadPlanDialogComponent;
  let fixture: ComponentFixture<CreateLoadPlanDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateLoadPlanDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLoadPlanDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
