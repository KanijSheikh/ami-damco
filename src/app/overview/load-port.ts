import {Injectable} from '@angular/core';

@Injectable()
export class LoadPort {
  constructor(public id: number, public name: string) { }
}
