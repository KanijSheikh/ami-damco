import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LpExceptionComponent } from './lp-exception.component';

describe('LpExceptionComponent', () => {
  let component: LpExceptionComponent;
  let fixture: ComponentFixture<LpExceptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LpExceptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LpExceptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
