import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTable, DataTableResource, DataTableTranslations} from 'angular-4-data-table-fix';
import {ActivatedRoute, Router} from '@angular/router';
import {ServerService} from '../../../server-service';
import {CountryDataService} from '../../../country-data-service';
import {GlobalService} from '../../../global-service';
import {CanComponentDeactivate} from '../../../can-deactivate-guard.service';
import {Subject} from 'rxjs/Subject';
import {LoadPlanDetailFilterService} from "../load-plan-detail-filter.service";
import {Subscription} from "rxjs/Subscription";
import {ApplicationDataService} from "../../../configuration/application-data.service";


@Component({
  selector: 'app-lp-exception',
  templateUrl: './lp-exception.component.html',
  styleUrls: ['./lp-exception.component.css']

})
export class LpExceptionComponent implements OnInit, CanComponentDeactivate {

  showSimulateDialog = false;
  showActionConfirmPopup = false;
  showDialog = false;
  po;
  allPoWithoutPagination;
  simulatedLaodPlan;

  ordersResource = new DataTableResource(this.po);
  simulatedResource = new DataTableResource(this.simulatedLaodPlan);
  orderCount = 0;
  simulatedCount = 0;
  id = 0;
  poId = 0;
  LPNumber: number;
  destinationSelect;
  LoadPlanNumber: number = this.globalService.LpNumber;

  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };

  exceptionActions = ['Change origin Service', 'Change Destination', 'Change mode of transportation', 'Change shipment status'];

  exceptionValues: string[];
  selectedAction = 'Change origin Service';
  selectedActionValues;

  cities = [];
  countries = [];
  regions = [];

  modeOfTransports;
  originServices;
  shipmentStatuses = ['Release cargo', 'Hold cargo'];

  cityAsDestination;


  public confirmAction = new Subject<boolean>();



  @ViewChild('poTable') poTable: DataTable;
  subscription: Subscription;

  filterVal = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: ''};

  constructor(private route: Router, private activatedRoute: ActivatedRoute, private serverService: ServerService,
              private countryDataService: CountryDataService, private globalService: GlobalService,
              private loadPlanDetailFilterService: LoadPlanDetailFilterService,
              private applicationDataService: ApplicationDataService) {

    this.subscription = this.loadPlanDetailFilterService.getFilterData().subscribe(val => {
      this.filterVal = val;
      this.ordersResource.query({}).then(orders => this.orderCount = orders.filter(this.filter(val, this)).length);
      this.reloadPo({'sortBy': 'poNumber', 'offset': '0', 'limit': '10'});
    });

    this.applicationDataService.getModeOfTransports().subscribe((response) => {
      this.modeOfTransports = response;
    });
    this.applicationDataService.getDamcoOriginServices().subscribe((response) => {
      this.originServices = response;
      this.onActionSelect(this.selectedAction);
    });


    this.serverService.getDestination().subscribe(
      (destinations) => {
        this.cityAsDestination = destinations;
        for (let destination of this.cityAsDestination) {
          this.cities.push(destination.destinationName);
        }
      }
    );

    this.countryDataService.getDestinationValues().forEach((item) => {
      if (item.destinationId === 2) {
        this.countries.push(item.name);
      } else if (item.destinationId === 3) {
        this.regions.push(item.name);
      }
    });

  }

  cellColor(car) {
    return 'rgb(255, 255,' + (155 + Math.floor(100 - ((car.rating - 8.7) / 1.3) * 100)) + ')';
  }

  ngOnInit() {
    this.LPNumber = this.activatedRoute.snapshot.params['id'];
    console.log(this.LPNumber);
    this.serverService.getPoForExceptionLp(this.LPNumber).subscribe(
      (containersOfExceptionLP: any[]) => {
        this.po = containersOfExceptionLP;
        this.po = [...this.po];
        this.allPoWithoutPagination = this.po;
        this.ordersResource = new DataTableResource(this.po);
        this.filterVal = this.loadPlanDetailFilterService.getLatestFilterValue();
        if (this.isFilterApplied()) {
          this.ordersResource.query({}).then(orders => this.orderCount = orders.filter(this.filter(this.filterVal, this)).length);
        }
        this.reloadPo({'sortBy': 'poNumber', 'offset': '0', 'limit': '10'});
        return this.po;
      }
    );

  }

  reloadPo(params) {

    if (this.po) {
      this.ordersResource.query(params).then(po => this.po = po);
    }

    if (this.isFilterApplied()) {
      const val = this.filterVal;
      this.ordersResource.query(params, this.filter(val, this)).then(po => {
        this.po = po;
      });

    } else {

      this.ordersResource.query(params).then(po => this.po = po);
      this.ordersResource.count().then(count => this.orderCount = count);
    }
  }


  isFilterApplied() {
    return this.filterVal.origin !== '' ||
      this.filterVal.carrier !== '' ||
      this.filterVal.lpNumber !== '' ||
      this.filterVal.destination !== '';
  }

  filter(val, thisArg) {

    return function (element) {
      return (val.carrier === '' || (element.carrier && element.carrier.toUpperCase().indexOf(val.carrier.toUpperCase()) > -1)) &&
        (val.origin === '' ||  element.loadPortCity.toUpperCase().indexOf(val.origin.toUpperCase()) > -1) &&
        (val.destination === '' || element.podCity.toUpperCase().indexOf(val.destination.toUpperCase()) > -1) &&
        (val.lpNumber === '' || element.poNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1
          || element.soNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1 );
    };
  }


  reloadSimulatedLp(params) {
    this.simulatedResource.query(params).then(simulatedLaodPlan => this.simulatedLaodPlan = simulatedLaodPlan);
  }

  onActionSelect(value) {
    if (value === 'Change Destination') {
      if (this.destinationSelect === 'City') {
        this.exceptionValues = this.cities.sort();
      } else if (this.destinationSelect === 'Country') {
        this.exceptionValues = this.countries.sort();
      } else if (this.destinationSelect === 'Region') {
        this.exceptionValues = this.regions.sort();
      }
    } else if (value === 'Change origin Service') {
      this.exceptionValues = this.originServices;
    } else if (value === 'Change mode of transportation') {
      this.exceptionValues = this.modeOfTransports;
    } else if (value === 'Change shipment status') {
      this.exceptionValues = this.shipmentStatuses;
    }
    this.selectedActionValues = '';
  }

  openDialog(rowEvent) {
    if (this.poTable.selectedRows.length > 0) {
      this.showDialog = !this.showDialog;
      this.destinationSelect = this.poTable.selectedRows[0].item.destinationSelect;
    }
  }


  updatePoAction(event) {
    let selectedPOIds = [];
    this.poTable.selectedRows.forEach((row) => {
      selectedPOIds.push(row.item.id);
    });

    this.allPoWithoutPagination.filter((po) => selectedPOIds.includes(po.id)).map((po => {
        po.updateAction = this.selectedActionValues;
        po.updateActionValue = this.selectedAction;


        if (this.selectedAction === 'Change origin Service') {
          po.originServiceType = this.selectedActionValues;
        }

        if (this.selectedAction === 'Change mode of transportation') {
          po.modOfTransport = this.selectedActionValues;
        }

        if (this.selectedAction === 'Change Destination') {
          if (this.destinationSelect === 'City') {
            po.podCity = this.selectedActionValues;
          } else if (this.destinationSelect === 'Country') {
            po.podCountry = this.selectedActionValues;
          } else if (this.destinationSelect === 'Region') {
            po.podRegion = this.selectedActionValues;
          }
        }
        return po;
      })
    );
    this.selectedAction = '';
    this.selectedActionValues = '';

  }

// delay(ms: number) {
//   return new Promise(resolve => setTimeout(resolve, ms));
// }

  simulate() {
    console.log('In Po===' + this.po);
    console.log('Simulated Po===' + this.allPoWithoutPagination);

    this.serverService.simulateAndCreateNewLP(this.allPoWithoutPagination, this.LPNumber).subscribe(response => {
      this.serverService.getSimulatedLoadPlan().subscribe((simulatedLp: any[]) => {
        this.simulatedLaodPlan = simulatedLp;
        this.simulatedLaodPlan = [...this.simulatedLaodPlan];
        this.simulatedResource = new DataTableResource(this.simulatedLaodPlan);
        this.simulatedResource.count().then(count => this.simulatedCount = count);
        this.reloadSimulatedLp({'sortBy': 'containerNumber', 'offset': '0', 'limit': '10'});
        this.rowColors(this.simulatedLaodPlan);
        return this.simulatedLaodPlan;
      });
    });
    this.showSimulateDialog = !this.showSimulateDialog;
  }

  confirm() {
    this.serverService.confirmLoadPlan(this.allPoWithoutPagination, this.LPNumber).subscribe(
      (response) => {
        for (let purchaseOrder of this.allPoWithoutPagination) {
          purchaseOrder.updateAction = false;
        }
        this.route.navigate(['/overview/exception']);
      }
    );

  }


  canDeactivate() {
    if (this.po) {
      for (let purchaseOrder of this.po) {
        if (purchaseOrder.updateAction) {
          this.showActionConfirmPopup = !this.showActionConfirmPopup;
          return this.confirmAction;
        }
      }
    }
    return true;
  }

  actionConfirm(val) {

    if (!val) {
      this.globalService.isOptimisedActive = false;
      this.globalService.isCarrierActive = false;
      this.globalService.isExceptionActive = true;
      this.confirmAction.next(false);
    } else {
      this.confirmAction.next(true);
    }

    return;
  }

  rowColors(simulatedContainers) {
    for (let purchaseOrders of simulatedContainers.purchaseOrderOp) {
      if (purchaseOrders.poStatus === 'exception') {
        return 'rgb(57,143,209)';
      }
    }
  }


}

