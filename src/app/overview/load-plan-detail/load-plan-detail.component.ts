import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {GlobalService} from "../../global-service";
import {ServerService} from "../../server-service";
import {IMyOptions} from "mydatepicker";
import {CountryDataService} from "../../country-data-service";
import {LoadPlanDetailFilterService} from "./load-plan-detail-filter.service";
import {ApplicationDataService} from "../../configuration/application-data.service";

@Component({
  selector: 'app-load-plan-detail',
  templateUrl: './load-plan-detail.component.html',
  styleUrls: ['./load-plan-detail.component.css']
})
export class LoadPlanDetailComponent implements OnInit {

  showDialog = false;
  showFilter = false;
  message;
  userName;
  notifications;
  id = 0;
  LoadPlanNumber: number = this.globalService.LpNumber;

  public myDatePickerOptions: IMyOptions = {
    dateFormat : 'yyyy-mm-dd',
  };

  transports;

  filterVal = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: '' };

  destinationsForFilter;
  originsForFilter;
  destinationCities: string[] = [];
  originCities: string[] = [];


  constructor(private route: Router, private globalService: GlobalService, private serverService: ServerService,
              private countryDataService: CountryDataService, private loadPlanDetailFilterService: LoadPlanDetailFilterService,
              private applicationDataService: ApplicationDataService) {

    this.serverService.getDestination().subscribe(
      (destinations) => {
        this.destinationsForFilter = destinations;
        for (let destination of this.destinationsForFilter) {
          if (!(this.destinationCities.includes(destination.destinationName))) {
            this.destinationCities.push(destination.destinationName);
            this.destinationCities.sort();
          }

        }
      }
    );

    this.serverService.getOrigin().subscribe(
      (origins) => {
        this.originsForFilter = origins;
        for (let origin of this.originsForFilter) {
          if (!(this.originCities.includes(origin.originName))) {
            this.originCities.push(origin.originName);
            this.originCities.sort();
          }
        }
      }
    );

    this.applicationDataService.getModeOfTransports().subscribe(
      (response) => {
        this.transports = response;
        this.transports.push('ALL');
        this.transports.sort();
      });
  }

  ngOnInit() {
    this.notifications = [];
    this.filterVal = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: '' };
     this.loadPlanDetailFilterService.clearLatestFilterValue();
  }

  onClear() {
    this.filterVal = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: '' };
    this.loadPlanDetailFilterService.sendFilterData(this.filterVal);
  }

  filterContent() {
    const val = this.filterVal;
    if (!val.cutOffDate) {
      val.cutOffDate = '';
    }
    this.loadPlanDetailFilterService.sendFilterData(val);
  }



  redirectOptimized(event) {
    this.globalService.isOptimisedActive = true;
    this.globalService.isExceptionActive = false;
    this.globalService.isCarrierActive = false;
    this.route.navigate(['overview/loadPlanDetail/optimized/', this.globalService.id]);
  }

  redirectCarrierBooking() {
    this.globalService.isCarrierActive = true;
    this.globalService.isOptimisedActive = false;
    this.globalService.isExceptionActive = false;
    this.route.navigate(['overview/loadPlanDetail/carrierBooking/', this.globalService.id]);
  }

  redirectException() {
    this.globalService.isExceptionActive = true;
    this.globalService.isCarrierActive = false;
    this.globalService.isOptimisedActive = false;
    this.route.navigate(['overview/loadPlanDetail/exception/', this.globalService.id]);
  }

  backToLP() {
    this.route.navigate(['overview/exception/']);
    this.globalService.isLpExceptionActive = true;
    this.globalService.isLpCompleted = false;
    this.globalService.isLpOptimisedActive = false;
  }

  sendNotification() {
    const body = '{ "createdBy" : ' + '"' + this.userName + '"' + ','
      + '"message" : ' + '"' + this.message + '"' + ','
      + '"loadplanId" : ' + '"' + this.LoadPlanNumber + '"'
      + '}';

    this.serverService.sendNotification(body).subscribe(response => {
      this.serverService.getNotification(this.LoadPlanNumber.toString()).subscribe((notification: any[]) => {
        return this.notifications = notification;
      });
    });
    this.message = '';
    this.userName = '';
  }

  getNotification() {
    console.log( 'Load No=' + this.LoadPlanNumber);
    this.serverService.getNotification(this.LoadPlanNumber.toString()).subscribe((notification: any[]) => {
      console.log('Notifications====' + notification);
     return this.notifications = notification;
    });
  }
}

