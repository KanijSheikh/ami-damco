import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LoadPlanDetailFilterService {

  filterValueSubject = new Subject<{}>();
  filterValue;

  constructor() { }

  sendFilterData(value: {}) {
    this.filterValue = value;
    this.filterValueSubject.next(value);
  }

  getFilterData(): Observable<any> {
    return this.filterValueSubject.asObservable();
  }

  getLatestFilterValue() {
    return this.filterValue;
  }

  clearLatestFilterValue() {
    this.filterValue = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: '' };
  }

}
