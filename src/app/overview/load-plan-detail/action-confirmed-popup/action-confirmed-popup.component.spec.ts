import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionConfirmedPopupComponent } from './action-confirmed-popup.component';

describe('ActionConfirmedPopupComponent', () => {
  let component: ActionConfirmedPopupComponent;
  let fixture: ComponentFixture<ActionConfirmedPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionConfirmedPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionConfirmedPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
