import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import {ServerService} from "../../../server-service";

@Component({
  selector: 'app-simulate-dialog',
  templateUrl: './simulate-dialog.component.html',
  styleUrls: ['./simulate-dialog.component.css'],
  animations: [
  trigger('dialog', [
    transition('void => *', [
      style({ transform: 'scale3d(.3, .3, .3)' }),
      animate(100)
    ]),
    transition('* => void', [
      animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
    ])
  ])
]
})
export class SimulateDialogComponent implements OnInit {

  showSimulateDialog = false;
  @Input() closable = true;
  @Input() visible: boolean;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private serverService: ServerService) { }

  ngOnInit() {
    this.serverService.getSimulatedLoadPlan();
  }

  close() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }

}
