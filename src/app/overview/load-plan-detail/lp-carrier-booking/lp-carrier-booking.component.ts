import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTable, DataTableTranslations, DataTableResource} from 'angular-4-data-table-fix';
import {ActivatedRoute} from '@angular/router';
import {ServerService} from '../../../server-service';
import {LoadPlanDetailFilterService} from '../load-plan-detail-filter.service';
import {Subscription} from 'rxjs/Subscription';


@Component({
  selector: 'app-lp-carrier-booking',
  templateUrl: './lp-carrier-booking.component.html',
  styleUrls: ['./lp-carrier-booking.component.css']

})
export class LpCarrierBookingComponent implements OnInit {
  containers;
  carrierBookingResource = new DataTableResource(this.containers);
  containerCount = 0;
  id = 0;
  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };

  @ViewChild(DataTable) filmsTable;
  subscription: Subscription;

  filterVal = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: ''};

  constructor(private activatedRoute: ActivatedRoute, private serverService: ServerService,
              private loadPlanDetailFilterService: LoadPlanDetailFilterService) {
    this.subscription = this.loadPlanDetailFilterService.getFilterData().subscribe(val => {
      this.filterVal = val;
      this.carrierBookingResource.query({}).then(orders => this.containerCount = orders.filter(this.filter(val, this)).length);
      this.reloadTable({'sortBy': 'containerNumber', 'offset': '0', 'limit': '10'});
    });
  }

  ngOnInit() {
    const LPNumber = this.activatedRoute.snapshot.params['id'];
    console.log(LPNumber);
    this.serverService.getContainersByConfirmedLp(LPNumber).subscribe(
      (containersOfExceptionLP: any[]) => {
        this.containers = containersOfExceptionLP;
        this.containers = [...this.containers];
        this.carrierBookingResource = new DataTableResource(this.containers);
        this.filterVal = this.loadPlanDetailFilterService.getLatestFilterValue();
        if (this.isFilterApplied()) {
          this.carrierBookingResource.query({}).then(orders => this.containerCount =
            orders.filter(this.filter(this.filterVal, this)).length);
        }
        this.reloadTable({'sortBy': 'containerNumber', 'offset': '0', 'limit': '10'});
        return this.containers;
      }
    );
  }

  reloadTable(params) {

    if (this.isFilterApplied()) {
      const val = this.filterVal;
      this.carrierBookingResource.query(params, this.filter(val, this)).then(cntrls => {
        this.containers = cntrls;
      });

    } else {

      this.carrierBookingResource.query(params).then(containers => this.containers = containers);
      this.carrierBookingResource.count().then(count => this.containerCount = count);
    }
  }


  isFilterApplied() {
    return this.filterVal.origin !== '' ||
      this.filterVal.carrier !== '' ||
      this.filterVal.lpNumber !== '' ||
      this.filterVal.destination !== '';
  }

  filter(val, thisArg) {

    return function (element) {
      return (val.carrier === '' || (element.carrier && element.carrier.toUpperCase().indexOf(val.carrier.toUpperCase()) > -1)) &&
        (val.origin === '' || thisArg.originMatch(element, val)) &&
        (val.destination === '' || thisArg.destinationMatch(element, val)) &&
        (val.lpNumber === '' || thisArg.quickSearch(element, val));
    };
  }

  quickSearch(container, val) {
    if (container.containerNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1) {
      return true;
    } else {
      for (const po of container.purchaseOrderOp) {
        if (po.poNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1 ||
          po.soNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1) {
          return true;
        }
      }
    }
    return false;
  }

  destinationMatch(element, val) {
    if (element.purchaseOrderOp && element.purchaseOrderOp[0].podCity &&
      element.purchaseOrderOp[0].podCity.toUpperCase().indexOf(val.destination.toUpperCase()) > -1) {
      return true;
    }
    return false;
  }

  originMatch(element, val) {
    if (element.purchaseOrderOp && element.purchaseOrderOp[0].loadPortCity
      && element.purchaseOrderOp[0].loadPortCity.toUpperCase().indexOf(val.origin.toUpperCase()) > -1) {
      return true;
    }
    return false;
  }

  cellColor(car) {
    return 'rgb(255, 255,' + (155 + Math.floor(100 - ((car.rating - 8.7) / 1.3) * 100)) + ')';
  }


}


