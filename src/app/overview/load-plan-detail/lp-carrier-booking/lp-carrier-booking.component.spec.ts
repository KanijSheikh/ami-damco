import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LpCarrierBookingComponent } from './lp-carrier-booking.component';

describe('LpCarrierBookingComponent', () => {
  let component: LpCarrierBookingComponent;
  let fixture: ComponentFixture<LpCarrierBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LpCarrierBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LpCarrierBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
