import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadPlanDetailComponent } from './load-plan-detail.component';

describe('LoadPlanDetailComponent', () => {
  let component: LoadPlanDetailComponent;
  let fixture: ComponentFixture<LoadPlanDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadPlanDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadPlanDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
