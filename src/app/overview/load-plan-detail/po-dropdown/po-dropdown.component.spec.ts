import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoDropdownComponent } from './po-dropdown.component';

describe('PoDropdownComponent', () => {
  let component: PoDropdownComponent;
  let fixture: ComponentFixture<PoDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
