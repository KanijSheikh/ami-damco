import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitPoDialogComponent } from './split-po-dialog.component';

describe('SplitPoDialogComponent', () => {
  let component: SplitPoDialogComponent;
  let fixture: ComponentFixture<SplitPoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitPoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitPoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
