import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DataTable, DataTableResource, DataTableTranslations} from 'angular-4-data-table-fix';
import {ActivatedRoute} from '@angular/router';
import {ServerService} from '../../../server-service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Carrier, DischargePort, SplitOption} from '../../container-size';
import {LoadPort} from '../../load-port';
import {IMyOptions} from 'mydatepicker';
import {LoadPlanDetailFilterService} from '../load-plan-detail-filter.service';
import {Subscription} from 'rxjs/Subscription';
import {SortablejsOptions} from "angular-sortablejs";
import {Observable} from "rxjs/Observable";
import {forEach} from "@angular/router/src/utils/collection";


@Component({
  selector: 'app-load-plan-optimised',
  templateUrl: './lp-optimised.component.html',
  styleUrls: ['./lp-optimised.component.css'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
  ]

})
export class LpOptimisedComponent implements OnInit, OnDestroy {

  @ViewChild(DataTable) filmsTable;

  notPlannedPo;
  showAddContainerDialog = false;
  showSplitPoDialog = false;
  containers;
  selectedContainersIds = [];
  purchaseOrders = []
  optimisedLPResource = new DataTableResource(this.containers);
  noPlannedPoResource = new DataTableResource(this.notPlannedPo);
  containerCount = 0;
  notPlannedPoCount = 0;
  id = 0;
  lpNumber: number;
  selectedContainerSize = '20FT';
  selectedLoadPort: LoadPort = new LoadPort(1, 'Phnom Penh');
  selectedDischargePort: DischargePort = new DischargePort(1, 'Shanghai');
  selectedCarrier: Carrier = new Carrier(1, 'MAEU');
  selectedSplitOption: SplitOption = new SplitOption(1, 'CBM');

  containerSize = [];
  containerTypes: any;

  loadPort = [
    new LoadPort(1, 'Phnom Penh'),
    new LoadPort(2, 'Yantian'),
    new LoadPort(3, 'Ho Chi Minh'),
    new LoadPort(4, 'Jakarta'),
    new LoadPort(5, 'Chennai'),
    new LoadPort(6, 'Port Qasim'),
    new LoadPort(7, 'Istanbul'),
  ];


  dischargePort = [
    new DischargePort(1, 'Shanghai'),
    new DischargePort(2, 'Busan'),
    new DischargePort(3, 'Tokyo'),
    new DischargePort(4, 'Memphis'),
    new DischargePort(5, 'Rotterdam'),
    new DischargePort(6, 'Antwerp'),
    new DischargePort(7, 'FelixStowe'),
  ];

  Carrier = [
    new Carrier(1, 'MAEU'),
    new Carrier(2, 'MAUSA'),
    new Carrier(3, 'MAAEB'),
  ];

  splitOption = [
    new SplitOption(1, 'CBM'),
    new SplitOption(2, 'KG')
  ];

  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };

  selectedETD;
  selectedETA;
  splitText;


  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };

  @ViewChild(DataTable) notPlannedPoTable;

  subscription: Subscription;

  filterVal = {carrier: '', origin: '', cutOffDate: '', mot: '', destination: '', lpNumber: ''};

  poMoved = false;
  poller;
  alertMessage: string;
  sortablejsOptions: SortablejsOptions = {
    group: 'orders',
    onEnd: (event: any) => {
      const fromContainer = event.from.id;
      const toContainer = event.to.id;
      const poID = event.item.id;
      if (fromContainer !== toContainer) { // when not moving in sme container
        this.movePo(fromContainer, toContainer, poID);
      }
    }
  };

  constructor(private activatedRoute: ActivatedRoute, private serverService: ServerService,
              private loadPlanDetailFilterService: LoadPlanDetailFilterService) {
    this.subscription = this.loadPlanDetailFilterService.getFilterData().subscribe(val => {
      this.filterVal = val;
      this.optimisedLPResource.query({}).then(orders => this.containerCount = orders.filter(this.filter(val, this)).length);
      this.reloadOptimisedContainer({'sortBy': 'containerNumber', 'offset': '0', 'limit': '10'});
    });

    this.serverService.getContainerTypes().subscribe(
      (response) => {
        this.containerTypes = response;
        for (const type of this.containerTypes) {
          this.containerSize.push(type.containerType);
        }
      },
      (error) => {
        this.containerSize = [];
      }
    );


    this.poller = Observable.interval(500).subscribe(x => {
      if (this.poMoved) {
        this.poMoved  = false;
        this.ngOnInit();
      }
    });
  }

  private getAllNotPlannedPo() {
    this.serverService.getAllNotPlannedPo(this.lpNumber).subscribe(
      (notPlannedPo: any[]) => {
        this.notPlannedPo = notPlannedPo;
        this.notPlannedPo = [...this.notPlannedPo];
        this.noPlannedPoResource = new DataTableResource(this.notPlannedPo);
        this.noPlannedPoResource.count().then(count => this.notPlannedPoCount = count);
        this.reloadNotPlannedPo({'sortBy': 'poNumber', 'offset': '0', 'limit': '15'});
        return this.notPlannedPo;
      }
    );
  }

  cellColor(car) {
    return 'rgb(255, 255,' + (155 + Math.floor(100 - ((car.rating - 8.7) / 1.3) * 100)) + ')';
  }

  ngOnInit() {
    this.lpNumber = this.activatedRoute.snapshot.params['id'];
    console.log(this.lpNumber);
    this.serverService.getContainersByOptimizedLP(this.lpNumber).subscribe(
      (containersOfExceptionLP: any[]) => {
        this.containers = containersOfExceptionLP;
        this.containers = [...this.containers];
        this.optimisedLPResource = new DataTableResource(this.containers);
        this.filterVal = this.loadPlanDetailFilterService.getLatestFilterValue();
        if (this.isFilterApplied()) {
          this.optimisedLPResource.query({}).then(orders => this.containerCount = orders.filter(this.filter(this.filterVal, this)).length);
        }
        this.reloadOptimisedContainer({'sortBy': 'containerNumber', 'offset': '0', 'limit': '10'});

        this.selectedContainersIds = [];
        this.purchaseOrders = []
        return this.containers;
      }
    );
  }

  ngOnDestroy() {
    this.poller.unsubscribe();
  }


  reloadOptimisedContainer(params) {
    if (this.isFilterApplied()) {
      const val = this.filterVal;
      this.optimisedLPResource.query(params, this.filter(val, this)).then(cntrls => {
        this.containers = cntrls;
      });

    } else {

      this.optimisedLPResource.query(params).then(containers => this.containers = containers);
      this.optimisedLPResource.count().then(count => this.containerCount = count);
    }
  }

  isFilterApplied() {
    return this.filterVal.origin !== '' ||
      this.filterVal.carrier !== '' ||
      this.filterVal.lpNumber !== '' ||
      this.filterVal.destination !== '';
  }

  filter(val, thisArg) {

    return function (element) {
      return (val.carrier === '' || (element.carrier && element.carrier.toUpperCase().indexOf(val.carrier.toUpperCase()) > -1)) &&
        (val.origin === '' || thisArg.originMatch(element, val) ) &&
        (val.destination === '' || thisArg.destinationMatch(element, val)) &&
        (val.lpNumber === '' || thisArg.quickSearch(element, val));
    };
  }

  quickSearch(container, val) {
    if (container.containerNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1) {
      return true;
    } else {
      for (const po of container.purchaseOrderOp) {
        if (po.poNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1 ||
          po.soNumber.toUpperCase().indexOf(val.lpNumber.toUpperCase()) > -1) {
          return true;
        }
      }
    }
    return false;
  }

  destinationMatch(element, val) {
    if (element.purchaseOrderOp && element.purchaseOrderOp[0].podCity &&
      element.purchaseOrderOp[0].podCity.toUpperCase().indexOf(val.destination.toUpperCase()) > -1) {
      return true;
    }
    return false;
  }

  originMatch(element, val) {
    if (element.purchaseOrderOp && element.purchaseOrderOp[0].loadPortCity &&
      element.purchaseOrderOp[0].loadPortCity.toUpperCase().indexOf(val.origin.toUpperCase()) > -1) {
      return true;
    }
    return false;
  }

  reloadNotPlannedPo(params) {
    this.noPlannedPoResource.query(params).then(notPlannedPo => this.notPlannedPo = notPlannedPo);
  }

  addContainer() {
    const body = '{ "containerSizeType" : ' + '"' + this.selectedContainerSize + '"' + ','
      + '"loadPort" : ' + '"' + this.selectedLoadPort.name + '"' + ','
      + '"dischargePort" : ' + '"' + this.selectedDischargePort.name + '"' + ','
      + '"carrier" : ' + '"' + this.selectedCarrier.name + '"' + ','
      + '"eta" : ' + '"' + this.selectedETA.formatted + '"' + ','
      + '"etd" : ' + '"' + this.selectedETD.formatted + '"'
      + '}';
    console.log(body);
    console.log(this.lpNumber);

    this.serverService.addContainer(body, this.lpNumber).subscribe(
      (response) => {
        this.ngOnInit();
      });


  }

  deleteContainer() {
    if (this.selectedContainersIds.length > 0) {
      let body = '[ ';
      for (let containerId of this.selectedContainersIds) {
        body = body.concat('{ "id" : ' + containerId + '} ,');
      }
      body = body.concat(']');
      body = body.replace(',]', ']');
      console.log(body);

      this.serverService.deleteContainer(body, this.lpNumber).subscribe(
        (response) => {
          this.ngOnInit();
        });
    }
  }

  rowClick(rowEvent) {
    this.selectedContainersIds = [];
    const event = rowEvent.event.currentTarget.className;
    if (event === 'data-table-row row-odd clickable' || event === 'data-table-row row-even clickable'
      || event === 'data-table-row clickable row-even' || event === 'data-table-row clickable row-odd') {
      this.selectedContainersIds.push(rowEvent.row.item.id);
    } else {
      this.selectedContainersIds = [];
    }

    console.log(this.selectedContainersIds);
  }

  addPo(rowEvent) {
    if (this.notPlannedPoTable.selectedRows.length > 0) {
      let body = '{ "id" : ' + this.selectedContainersIds[0] + ',' + '"purchaseOrderOp" : [ ';
      for (let selectedRow of this.notPlannedPoTable.selectedRows) {
        body = body.concat('{ "id" : ' + selectedRow.item.id + '},');
      }
      body = body.concat('] }');
      body = body.replace(',] }', '] }');
      console.log(body);

      this.serverService.addPoToContainer(body).subscribe(
        (response) => {
          this.ngOnInit();
        });
    }
  }

  // rowAddPoClick(rowEvent) {
  //   const event = rowEvent.event.currentTarget.className;
  //   if (event === 'data-table-row row-odd clickable' || event === 'data-table-row row-even clickable') {
  //     this.selectedPoIds.push(rowEvent.row.item.id);
  //   } else {
  //     this.selectedPoIds.splice(this.selectedPoIds.indexOf(rowEvent.row.item.id));
  //   }
  //  console.log(this.selectedPoIds);
  // }

  checkAll(ev, selectedPos) {
    selectedPos.forEach(x => x.updateAction = ev.target.checked);
    for (let selectedPo of selectedPos) {
      let duplicatePo = this.purchaseOrders.filter((item) => item.id == selectedPo.id);
      if (selectedPo.updateAction) {
        if (duplicatePo.length === 0) {
          this.purchaseOrders.push(selectedPo);
        }
      } else {
        this.purchaseOrders.splice(this.purchaseOrders.indexOf(this.purchaseOrders.filter((item) => item.id == selectedPo.id)));
      }

    }
  }

  checkOne(ev, po) {
    po.updateAction = ev.target.checked;
    if (po.updateAction) {
      this.purchaseOrders.push(po);
    } else {
      this.purchaseOrders.splice(this.purchaseOrders.indexOf(this.purchaseOrders.filter((item) => item.id == po.id)));
    }
  }

  isAllChecked(selectedPos) {
    return (selectedPos.every(_ => _.updateAction));
  }

  deletePurchaseOrder() {
    this.serverService.deletePoFromContainer(this.purchaseOrders.filter((item) => item.updateAction === true)).subscribe(
      (response) => {
        this.purchaseOrders = [];
        this.ngOnInit();
      });
  }

  splitPo() {
    this.purchaseOrders.map((po => {
        po.updateField = this.selectedSplitOption.name;
        po.updateValue = this.splitText;
        return po;
      })
    );
    this.serverService.splitPurchaseOrder(this.purchaseOrders).subscribe(
      (response) => {
        this.purchaseOrders.splice(this.purchaseOrders.indexOf(this.purchaseOrders.filter((item) => item.updateAction === true)));
        this.ngOnInit();
      });
  }

  getAllPo() {
    this.getAllNotPlannedPo();
  }

  movePo(fromContainer, toContainer, poID) {
   const PO =  this.getMatchingPOObject( poID);
    if (PO != null) {
      const poArray = [];
      poArray.push(PO);
      const body = '{ "id" : ' + toContainer + ',' + '"purchaseOrderOp" :' + JSON.stringify(poArray) + ' }';
      console.log(body);

      this.serverService.movePoToContainer(body).subscribe(
        (res) => {
          this.poMoved = true;
        },
        (error) => {
          console.log(error);
          this.alertMessage = error.message;
          this.poMoved = true;
        });
    }

  }

  getMatchingPOObject( poID) {
    for (const cont of this.containers) {
        for (const po of cont.purchaseOrderOp) {
          if (po.id == poID) {
            return po;
          }
        }
    }
    return null;
  }

  clearAlert() {
    this.alertMessage = null;
  }


}

