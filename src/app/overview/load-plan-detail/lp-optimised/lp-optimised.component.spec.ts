import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LpOptimisedComponent } from './lp-optimised.component';

describe('LpOptimisedComponent', () => {
  let component: LpOptimisedComponent;
  let fixture: ComponentFixture<LpOptimisedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LpOptimisedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LpOptimisedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
