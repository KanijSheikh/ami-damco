import {Injectable} from '@angular/core';

@Injectable()
export class Destination {
  constructor(public id: number, public name: string) { }
}
