import {Injectable} from '@angular/core';

@Injectable()
export class ExceptionAction {
  constructor(public id: number, public name: string) { }
}
