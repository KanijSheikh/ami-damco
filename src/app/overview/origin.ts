import {Injectable} from '@angular/core';

@Injectable()
export class Origin {
  constructor(public id: number, public name: string) { }
}
