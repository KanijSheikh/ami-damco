import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptimisedComponent } from './optimised.component';

describe('OptimisedComponent', () => {
  let component: OptimisedComponent;
  let fixture: ComponentFixture<OptimisedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptimisedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptimisedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
