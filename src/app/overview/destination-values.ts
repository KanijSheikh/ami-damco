import {Injectable} from '@angular/core';

@Injectable()
export class DestinationValues {
  constructor(public id: number, public destinationId: number , public name: string) { }
}
