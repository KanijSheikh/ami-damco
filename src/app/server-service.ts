import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {WareHouse} from './configuration/warehouse/warehouse.component';
import {AllowedContainer} from './configuration/allowed-container/allowed-container.component';

@Injectable()
export class ServerService {
  status: string;
  prodUrlPrefix: string;
  headers = new HttpHeaders({'content-type': 'application/json'});

  constructor(private http: HttpClient, private route: Router) {
    // this.prodUrlPrefix = 'ami/';
   this.prodUrlPrefix = '';
  }

  storeRules(servers: String) {

    return this.http.post(this.prodUrlPrefix + 'api/createRule',
      servers,
      {headers: this.headers, observe: 'response'}
    );
  }

  copyDecisionTree(dtid) {
    if (dtid != null && dtid !== 0) {
      const url = this.prodUrlPrefix + 'api/copyRule?ruleId=' + dtid;
      return this.http.post(url, '{"ruleId" : "' + dtid + '"}', { headers: this.headers});
    }
    return null;
  }

  deleteDecisionTree(dtid) {
    if (dtid != null && dtid !== 0) {
      const url = this.prodUrlPrefix + 'api/deleteRule/' + dtid;
      return this.http.delete(url);
    }
    return null;
  }

  reOrderDecisionTree(ruleId, oldOrder, newOrder) {
    const url = this.prodUrlPrefix + 'api/rules/reorder?ruleId=' + ruleId + '&oldOrder=' + oldOrder + '&newOrder=' + newOrder;
    return this.http.post(url, '', { headers: this.headers});
  }

  getAllRulesSummary() {
    return this.http.get(this.prodUrlPrefix + 'api/getAllRules');
  }

  getRules() {
    return this.http.get(this.prodUrlPrefix + 'api/rules');
  }

  getPurchaseOrders() {
    return this.http.get(this.prodUrlPrefix + 'api/getAllPo');
  }

  getExceptionLoadPlan() {
    return this.http.get(this.prodUrlPrefix + 'api/getLoadPlan?status=Exception');
  }

  getOptimizedLoadPlan() {
    return this.http.get(this.prodUrlPrefix + 'api/getLoadPlan?status=optimized');
  }


  getCompletedLoadPlan() {
    return this.http.get(this.prodUrlPrefix + 'api/getLoadPlan?status=Completed');
  }


  getContainersByExceptionLP(lpNumber) {
    return this.http.get(this.prodUrlPrefix + 'api/getContainerByStatus?id=' + lpNumber + '&status=exception');
  }

  getContainersByConfirmedLp(lpNumber) {
    return this.http.get(this.prodUrlPrefix + 'api/getContainerByStatus?id=' + lpNumber);
  }

  getContainersByOptimizedLP(lpNumber) {
    return this.http.get(this.prodUrlPrefix + 'api/getContainerByStatus?id=' + lpNumber);
  }

  getPoForExceptionLp(lpNumber) {
    return this.http.get(this.prodUrlPrefix + 'api/getAllPo?id=' + lpNumber + '&status=exception');
  }


  createLoadPlan(servers: String) {

    return this.http.post(this.prodUrlPrefix + 'api/createLoadPlan',
      servers,
      {headers : this.headers, observe: 'response'}
    );
  }

  createScheduledLoadPlan(servers: String) {

    return this.http.post(this.prodUrlPrefix + 'api/scheduleLoadPlan',
      servers,
      {headers : this.headers, observe: 'response'}
    );
  }


  simulateAndCreateNewLP(pos: String, lpNumber: Number) {
    return this.http.post(this.prodUrlPrefix + 'api/saveSimulatedLoadPlan?loadPlanId=' + lpNumber,
      pos,
      {headers: this.headers}
    );
      // .subscribe(
      //   (response) => console.log(response),
      //   (error) => console.log(error)
      // );
  }

  getSimulatedLoadPlan() {
    return this.http.get(this.prodUrlPrefix + 'api/getSimulatedLoadPlan');
  }


  confirmLoadPlan(pos: String, lpNumber: Number) {
    return this.http.post(this.prodUrlPrefix + 'api/confirmLoadPlan?loadPlanId=' + lpNumber,
      pos,
      {headers: this.headers}
    );
  }


  uploadPurchaseOrder(files: File, fileType: string) {
    const headers = new HttpHeaders();
    const formdata: FormData = new FormData();
    formdata.append('file', files);
    formdata.append('fileType', fileType);
    return this.http.post(this.prodUrlPrefix + 'api/uploadPurchaseOrder', formdata,
      {headers: headers}
    );
  }

  addContainer(container: String, lpNumber: Number) {

    return this.http.post(this.prodUrlPrefix + 'api/addContainer?loadPlanId=' + lpNumber,
      container,
      {headers : this.headers}
    );
  }

  deleteContainer(containers: String, lpNumber: Number) {

    return this.http.post(this.prodUrlPrefix + 'api/deleteContainer?loadPlanId=' + lpNumber,
      containers,
      {headers : this.headers}
    );
  }

  getAllNotPlannedPo(lpNumber: Number) {
    return this.http.get(this.prodUrlPrefix + 'api/getAllPoForLoadPlan?id=' + lpNumber);
  }

  addPoToContainer(container: String) {

    return this.http.post(this.prodUrlPrefix + 'api/addPurchaseOrder',
      container,
      {headers : this.headers}
    );
  }

  deletePoFromContainer(po: any) {

    return this.http.post(this.prodUrlPrefix + 'api/deletePurchaseOrder',
      po,
      {headers : this.headers}
    );
  }

  splitPurchaseOrder(po: any) {

    return this.http.post(this.prodUrlPrefix + 'api/splitPurchaseOrder',
      po,
      {headers : this.headers}
    );
  }

  movePoToContainer(container: String) {

    return this.http.post(this.prodUrlPrefix + 'api/movePurchaseOrder',
      container,
      {headers : this.headers}
    );
  }

  sendNotification(message: string) {
    return this.http.post(this.prodUrlPrefix + 'api/createMessage',
      message,
      {headers: this.headers}
    );
  }

  getNotification(loadPlanNumber: string) {
    return this.http.get(this.prodUrlPrefix + 'api/getMessages?loadplanId=' + loadPlanNumber).map(
      (response) => {
        return response;
      }
    );
  }

  uploadLocations(locations: File) {
    const headers = new HttpHeaders()
    const formdata: FormData = new FormData();
    formdata.append('file', locations);
    return this.http.post(this.prodUrlPrefix + 'api/uploadLocation', formdata,
      {headers: headers}
    );
  }

  getLocation() {
    return this.http.get(this.prodUrlPrefix + 'api/getLocation');
  }


  deleteLocation(location: any) {
    const headers = new HttpHeaders({'content-type' : 'application/json'})
    return this.http.post(this.prodUrlPrefix + 'api/deleteLocation',
      location,
      {headers : headers}
    );
  }

  addLocation(location: String) {
    const headers = new HttpHeaders({'content-type' : 'application/json'})
    return this.http.post(this.prodUrlPrefix + 'api/addLocation',
      location,
      {headers : headers, observe: 'response'}
    );
  }

  getApplicationData(type: String) {
    return this.http.get(this.prodUrlPrefix + 'api/applicationData?type=' + type);
  }

  saveApplicationData(type: String, values: string[]) {
    return this.http.post(this.prodUrlPrefix + 'api/applicationData?type=' + type,
      values, {headers : this.headers}
    );
  }


  getContainerTypes() {
    return this.http.get(this.prodUrlPrefix + 'api/applicationData/containerType');
  }

  saveContainerTypes( values: any) {
    return this.http.post(this.prodUrlPrefix + 'api/applicationData/containerType',
      values, {headers : this.headers}
    );
  }


  uploadDestination(destinations: File) {
    const headers = new HttpHeaders()
    const formdata: FormData = new FormData();
    formdata.append('file', destinations);
    return this.http.post(this.prodUrlPrefix + 'api/uploadDestination', formdata,
      {headers: headers}
    );
  }

  getDestination() {
    return this.http.get(this.prodUrlPrefix + 'api/getDestination');
  }


  deleteDestination(destination: any) {
    const headers = new HttpHeaders({'content-type' : 'application/json'})
    return this.http.post(this.prodUrlPrefix + 'api/deleteDestination',
      destination,
      {headers : headers}
    );
  }

  addDestination(destination: String) {
    const headers = new HttpHeaders({'content-type' : 'application/json'})
    return this.http.post(this.prodUrlPrefix + 'api/addDestination',
      destination,
      {headers : headers, observe: 'response'}
    );
  }


  uploadContainerFillRate(containers: File) {
    const headers = new HttpHeaders()
    const formdata: FormData = new FormData();
    formdata.append('file', containers);
    return this.http.post(this.prodUrlPrefix + 'api/uploadContainerFillRate', formdata,
      {headers: headers}
    );
  }

  getContainerFillRate() {
    return this.http.get(this.prodUrlPrefix + 'api/getContainerFillRate');
  }


  deleteContainerFillRate(container: any) {
    const headers = new HttpHeaders({'content-type' : 'application/json'})
    return this.http.post(this.prodUrlPrefix + 'api/deleteContainerFillRate',
      container,
      {headers : headers}
    );
  }

  addContainerFillRate(container: String) {
    const headers = new HttpHeaders({'content-type' : 'application/json'})
    return this.http.post(this.prodUrlPrefix + 'api/addContainerFillRate',
      container,
      {headers : headers, observe: 'response'}
    );
  }


  uploadOrigin(origins: File) {
    const headers = new HttpHeaders()
    const formdata: FormData = new FormData();
    formdata.append('file', origins);
    return this.http.post(this.prodUrlPrefix + 'api/uploadOrigin', formdata,
      {headers: headers}
    );
  }

  getOrigin() {
    return this.http.get(this.prodUrlPrefix + 'api/getOrigin');
  }


  deleteOrigin(origin: any) {
    const headers = new HttpHeaders({'content-type' : 'application/json'})
    return this.http.post(this.prodUrlPrefix + 'api/deleteOrigin',
      origin,
      {headers : headers}
    );
  }

  addOrigin(origin: String) {
    const headers = new HttpHeaders({'content-type' : 'application/json'})
    return this.http.post(this.prodUrlPrefix + 'api/addOrigin',
      origin,
      {headers : headers, observe: 'response'}
    );
  }

  uploadVesselSchedule(vesselSchedules: File) {
    const headers = new HttpHeaders()
    const formdata: FormData = new FormData();
    formdata.append('file', vesselSchedules);
    return this.http.post(this.prodUrlPrefix + 'api//uploadVesselSchedule', formdata,
      {headers: headers}
    );
  }
  uploadWarehouse(file: File) {
    const headers = new HttpHeaders();
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    return this.http.post(this.prodUrlPrefix + 'api/warehouse/upload', formdata,
      {headers: headers}
    );
  }

  getVesselSchedule() {
    return this.http.get(this.prodUrlPrefix + 'api/getVesselSchedule');
  }


  deleteVesselSchedule(vesselSchedule: any) {
    const headers = new HttpHeaders({'content-type' : 'application/json'})
    return this.http.post(this.prodUrlPrefix + 'api/deleteVesselSchedule',
      vesselSchedule,
      {headers : headers}
    );
  }

  addVesselSchedule(vesselSchedule: String) {
    const headers = new HttpHeaders({'content-type' : 'application/json'})
    return this.http.post(this.prodUrlPrefix + 'api/addVesselSchedule',
      vesselSchedule,
      {headers : headers, observe: 'response'}
    );
  }
  getWarehouses() {
    return this.http.get(this.prodUrlPrefix + 'api/warehouse');
  }


  deleteWarehouses(ids: any) {
    const headers = new HttpHeaders({'content-type' : 'application/json'});
    return this.http.post(this.prodUrlPrefix + 'api/warehouse/delete', ids, {headers : headers});
  }

  addWarehouse(warehouse: WareHouse) {
    const headers = new HttpHeaders({'content-type' : 'application/json'});
    return this.http.post(this.prodUrlPrefix + 'api/warehouse',
      warehouse,
      {headers : headers}
    );
  }

  uploadAllowedContainers(file: File) {
    const headers = new HttpHeaders();
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    return this.http.post(this.prodUrlPrefix + 'api/allowedContainer/upload', formdata,
      {headers: headers}
    );
  }

  getAllowedContainers() {
    return this.http.get(this.prodUrlPrefix + 'api/allowedContainer');
  }


  deleteAllowedContainers(ids: any) {
    const headers = new HttpHeaders({'content-type' : 'application/json'});
    return this.http.post(this.prodUrlPrefix + 'api/allowedContainer/delete', ids, {headers : headers});
  }

  addAllowedContainer(allowedContainer: AllowedContainer) {
    const headers = new HttpHeaders({'content-type' : 'application/json'});
    return this.http.post(this.prodUrlPrefix + 'api/allowedContainer',
      allowedContainer,
      {headers : headers}
    );
  }
}

