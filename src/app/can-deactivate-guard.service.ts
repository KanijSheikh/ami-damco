import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {LpExceptionComponent} from './overview/load-plan-detail/lp-exception/lp-exception.component';
import {GlobalService} from './global-service';

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}


export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {

  canDeactivate(component: LpExceptionComponent, route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot, nextState: RouterStateSnapshot) :
    Observable<boolean> | Promise<boolean> | boolean{
    return component.canDeactivate();
  }

}
