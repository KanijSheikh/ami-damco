import {Injectable} from '@angular/core';

@Injectable()
export class GlobalService {

  id = 0;
  LpNumber = 0;

  isExceptionActive: boolean = true;
  isOptimisedActive: boolean = false;
  isCarrierActive: boolean = false;

  isLpExceptionActive: boolean = true;
  isLpOptimisedActive: boolean = false;
  isLpCompleted: boolean = false;

  selectedDecisionTreeId  = "";

  isActionEdit = false;

  decisionTreeName: string;
}
