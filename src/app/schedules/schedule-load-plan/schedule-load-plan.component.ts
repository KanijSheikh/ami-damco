import {Component, OnInit} from '@angular/core';
import {OriginValues} from '../../overview/origin-values';
import {DestinationValues} from '../../overview/destination-values';
import {Origin} from '../../overview/origin';
import {Destination} from '../../overview/destination';
import {IMyOptions} from 'mydatepicker';
import {CountryDataService} from '../../country-data-service';
import {ApplicationDataService} from '../../configuration/application-data.service';
import {ServerService} from "../../server-service";

@Component({
  selector: 'app-schedule-load-plan',
  templateUrl: './schedule-load-plan.component.html',
  styleUrls: ['./schedule-load-plan.component.css']
})
export class ScheduleLoadPlanComponent implements OnInit {

  finalOriginValues: OriginValues[];
  finalDestinationValues: DestinationValues[];

  selectedModeOfTransport = 'ALL';

  transports;

  selectedRepeatLp = 'Daily';

  repeatsLp = ['Daily', 'Weekly']

  selectedServiceType = 'ALL';
  serviceTypes;

  selectedOrigin: Origin = new Origin(1, 'City');

  origins: Origin[];

  originValues;

  selectedOriginValues;
  selectedDestinationValues;

  selectedDestination: Destination = new Destination(1, 'City');
  destinations: Destination[];
  destinationValues;

  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'yyyy-mm-dd',
  };

  exceptionDate: string;
  exceptionDates: string[] = ['-7', '-6', '-5', '-4', '-3', '-2', '-1', '0', '1', '2', '3', '4', '5', '6', '7'];

  days: string[] = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  selectedDays: string[] = [];

  dropdownSettings = {
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    itemsShowLimit: 2,
    allowSearchFilter: true
  };

  dropdownSettingsForOrigin = {
    singleSelection: true,
    idField: 'id',
    textField: 'name',
    itemsShowLimit: 2,
    allowSearchFilter: true
  };

  selectedCuttOffDate;
  startTime;
  startDate;
  repeat;
  selectedEndDate;
  endDate;
  occurences;

  originCities: string[] = [];
  destinationCities: string[] = [];

  constructor(private countryDataService: CountryDataService, private applicationDataService: ApplicationDataService, private serverService: ServerService) {
    this.origins = this.countryDataService.getOrigin();
    this.destinations = this.countryDataService.getDestination();

    this.serverService.getOrigin().subscribe(
      (origins) => {
        this.originValues = origins;
        for (let origin of this.originValues) {
          if (!(this.originCities.includes(origin.originName))) {
            this.originCities.push(origin.originName);
            this.originCities.sort();
            this.originCities = [...this.originCities];
          }
        }
      }
    );

    this.serverService.getDestination().subscribe(
      (destinations) => {
        this.destinationValues = destinations;
        for (let destination of this.destinationValues) {
          if (!(this.destinationCities.includes(destination.destinationName))) {
            this.destinationCities.push(destination.destinationName);
            this.destinationCities.sort();
            this.destinationCities = [...this.destinationCities];
          }

        }
      }
    );

    this.applicationDataService.getModeOfTransports().subscribe(
      (response) => {
        this.transports = response;
        this.transports.push('ALL');
        this.transports.sort();
      });

    this.applicationDataService.getDamcoOriginServices().subscribe(
      (response) => {
        this.serviceTypes = response;
        this.serviceTypes.push('ALL');
        this.serviceTypes.sort();
      });
  }


  ngOnInit() {
  }

  onOriginChange() {
  }

  onDestinationChange() {

  }


  onOriginSelect(originid) {
    this.selectedOriginValues = [];
    if (originid === '1') {
      this.originCities = [...this.originCities];
    } else if (originid === '2') {
      this.originCities = ['Vietnam', 'China', 'Camododia', 'Turkey', 'Sri Lanka', 'India', 'Indonesia'];
    } else if (originid === '3') {
      this.originCities = ['Asia', 'Imea', 'Europe', 'Americas'];
    }
  }

  onDestinationSelect(destinationId) {
    this.selectedDestinationValues = [];
    if (destinationId === '1') {
      this.destinationCities = [...this.destinationCities];
    } else if (destinationId === '2') {
      this.destinationCities = ['China', 'Korea', 'Japan', 'USA', 'Netherlands', 'Belgium', 'Germany', 'France', 'United Kingdom'];
    } else if (destinationId === '3') {
      this.destinationCities = ['Greater China', 'Apla', 'Emea', 'North America'];
    }
  }


  onDayChange(e, day: string): void {
    if (e.target.checked) {
      this.selectedDays.push(day);
    }
    else {
      const index = this.selectedDays.indexOf(day);

      if (index > -1) {
        this.selectedDays.splice(index, 1);
      }
    }
  }

  scheduleLoadPlan() {

    const originNames = [];
    for (const obj of this.selectedOriginValues) {
      this.finalOriginValues = this.countryDataService.getOriginValues().filter((item) => item.id === obj.id);
      for (let finalOriginValue of this.finalOriginValues) {
        originNames.push('"' + finalOriginValue.name + '"');

      }
    }


    const destinationNames = [];
    for (const obj of this.selectedDestinationValues) {
      this.finalDestinationValues = this.countryDataService.getDestinationValues().filter((item) => item.id === obj.id);
      for (let finalDestValue of this.finalDestinationValues) {
        destinationNames.push('"' + finalDestValue.name + '"');

      }
    }
    console.log('Date======== ' + this.selectedCuttOffDate.formatted);


    this.origins = this.countryDataService.getOrigin().filter((item) => item.id == this.selectedOrigin.id);
    this.destinations = this.countryDataService.getDestination().filter((item) => item.id == this.selectedDestination.id);


    const body = '{ "serviceType" : ' + '"' + this.selectedServiceType + '"' + ','
      + '"loadPlanNumber" : ' + '"Lp-100"' + ','
      + '"loadPlanName" : ' + '"LPNAME-1"' + ','
      + '"modeOfTransport" : ' + '"' + this.selectedModeOfTransport + '"' + ','
      + '"cutOff" : ' + '"' + this.selectedCuttOffDate.formatted + '"' + ','
      + '"startDate" : ' + '"' + this.selectedCuttOffDate.formatted + '"' + ','
      + '"startTime" : ' + '"' + this.startTime + '"' + ','
      + '"repeat" : ' + '"' + this.selectedRepeatLp + '"' + ','
      + '"repeatOccurences" : ' + this.repeat + ','
      + '"days" : [' + '"' + 'saturday' + '"' + '] ,'
      + '"endDate" : ' + '"' + this.endDate.formatted + '"' + ','
      + '"noEndDate" : ' + '"' + this.selectedEndDate + '"' + ','
      + '"endAfterOccurences" : ' + this.occurences + ','
      + '"originDto" : {' + '"' + 'type' + '" : ' + '"' + this.origins[0].name + '"' + ',' +
      '"originValue": [ ' + originNames + ' ] } ,'
      + '"destinationDto" : {' + '"' + 'type' + '" : ' + '"' + this.destinations[0].name + '"' + ',' +
      '"destinationValue": [ ' + destinationNames + ' ] }' + '}';

    console.log(body);
  }

}
