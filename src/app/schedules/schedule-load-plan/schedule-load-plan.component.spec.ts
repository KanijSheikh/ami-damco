import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleLoadPlanComponent } from './schedule-load-plan.component';

describe('ScheduleLoadPlanComponent', () => {
  let component: ScheduleLoadPlanComponent;
  let fixture: ComponentFixture<ScheduleLoadPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleLoadPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleLoadPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
