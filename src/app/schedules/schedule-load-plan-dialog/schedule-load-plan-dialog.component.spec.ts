import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleLoadPlanDialogComponent } from './schedule-load-plan-dialog.component';

describe('ScheduleLoadPlanDialogComponent', () => {
  let component: ScheduleLoadPlanDialogComponent;
  let fixture: ComponentFixture<ScheduleLoadPlanDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleLoadPlanDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleLoadPlanDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
