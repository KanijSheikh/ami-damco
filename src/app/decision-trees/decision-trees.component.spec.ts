import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecisionTrees } from './decision-trees.component';

describe('DecisionTrees', () => {
  let component: DecisionTrees;
  let fixture: ComponentFixture<DecisionTrees>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecisionTrees ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecisionTrees);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
