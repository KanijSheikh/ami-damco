import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDecisionTreeDialogComponent } from './create-decision-tree-dialog.component';

describe('CreateDecisionTreeDialogComponent', () => {
  let component: CreateDecisionTreeDialogComponent;
  let fixture: ComponentFixture<CreateDecisionTreeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDecisionTreeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDecisionTreeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
