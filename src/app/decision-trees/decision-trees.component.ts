import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ServerService} from '../server-service';
import {GlobalService} from '../global-service';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {DataTableDirective} from "angular-datatables";
import {SortablejsOptions} from "angular-sortablejs";
import {Observable} from "rxjs/Rx";

@Component({
  selector: 'app-decision-trees',
  templateUrl: './decision-trees.component.html',
  styleUrls: ['./decision-trees.component.css']
})
export class DecisionTreesComponent implements OnInit, OnDestroy {

  allTrees: decisionTreeSummary [];
  decisionTreeName;
  selectedDT;
  selectedDecisionTreeId: number;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  poller;
  pageLength = 10;

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  orderingDetails: {id: number, oldIndex: number, newIndex: number} = {id: 0, oldIndex: 0, newIndex: 0};

  options: SortablejsOptions = {
    onEnd: (event: any) => {

      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        const pageStart =  dtInstance.page.info().start;
        this.orderingDetails = { id: event.item.id, oldIndex: pageStart + event.oldIndex + 1, newIndex: pageStart + event.newIndex + 1};
      });
    }
  };

  constructor(private router: Router, private serverService: ServerService, private globalService: GlobalService) {
    this.allTrees = [];
    this.poller = Observable.interval(700).subscribe(x => {
      this.onSave();
    });
  }

  ngOnInit() {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: this.pageLength,
      searching: false,
      order: [0, 'asc'],
      rowCallback: (row: Node, data: any, index: number) => {
        $('td', row).unbind('click');
        $('td', row).bind('click', () => {
            this.selectedDT = data;
            this.selectedDecisionTreeId = data.DT_RowId;
            this.selectedDT.id = data.DT_RowId;
        });
        return row;
      }
    };

    this.getAllDecisionTrees();

  }

  ngOnDestroy() {
    this.poller.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }


  getAllDecisionTrees() {
    this.serverService.getAllRulesSummary().subscribe(
      (response) => {
        const data = response;
        const count = Object.keys(data).length;
        this.allTrees = [];
        this.selectedDecisionTreeId = null;
        this.globalService.selectedDecisionTreeId = '';
        let index = 0;
        for (; index < count; index++) {
          const dts = new decisionTreeSummary();
          dts.templateName = data[index].ruleName;
          dts.createdBy = data[index].createdBy;
          dts.label = 'no label';
          dts.status = 'Active';
          dts.order = data[index].treeOrder;
          dts.id = data[index].id;
          this.allTrees.push(dts);
        }
        this.dtTrigger.next();
      },
      (error) => {
        this.allTrees = [];
        this.dtTrigger.next();
      }
    );
  }

  deleteAndUpdateDecisionTreeList() {
    if (this.selectedDT != null) {
     this.serverService.deleteDecisionTree(this.selectedDT.id).subscribe(
          (response) => {
            const data = response;
            this.rerender();
            this.getAllDecisionTrees();
          }
        );
    }
    this.selectedDT = null;
    this.selectedDecisionTreeId = null;
  }

  copyAndUpdateDecisionTreeList() {
    if (this.selectedDT != null) {
      this.serverService.copyDecisionTree(this.selectedDT.id).subscribe(
          (response) => {
            console.log(response);
            this.rerender();
            this.getAllDecisionTrees();

          }
        );
      this.selectedDT = null;
      this.selectedDecisionTreeId = null;
    }
  }

  editDecisionTree() {
    if (this.selectedDT != null) {
      this.globalService.selectedDecisionTreeId = this.selectedDT.id.toString();
      this.selectedDT = null;
      this.globalService.isActionEdit = true;
      this.router.navigate(['decisionTrees/matchingRule']);
    }
  }

  createDecisionTree() {
    this.globalService.isActionEdit = false;
    this.globalService.decisionTreeName = this.decisionTreeName;
    this.router.navigate(['decisionTrees/matchingRule']);
  }

  onSave() {
    if (this.orderingDetails.id !== 0) {
     this.serverService.reOrderDecisionTree(this.orderingDetails.id, this.orderingDetails.oldIndex,
       this.orderingDetails.newIndex).subscribe(
       (response) => {
         this.orderingDetails = {id: 0, oldIndex: 0, newIndex: 0};
         this.rerender();
         this.getAllDecisionTrees();
       },
       (error) => {
         this.orderingDetails = {id: 0, oldIndex: 0, newIndex: 0};
       }
     );
   }
  }
}



class decisionTreeSummary {
  templateName: string;
  createdBy: string;
  label: string;
  status: string;
  order: number;
  checked: boolean;
  id: number;

  constructor() {
    this.templateName = '';
    this.createdBy = '';
    this.status = '';
    this.order = 0;
    this.label = '';
    this.checked = true;
    this.id = 0;
  }
}
