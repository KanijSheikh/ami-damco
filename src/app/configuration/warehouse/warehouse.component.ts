import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTable, DataTableResource, DataTableTranslations} from 'angular-4-data-table-fix';
import {ServerService} from '../../server-service';
import {ExcelService} from '../excel.service';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.css']
})
export class WarehouseComponent implements OnInit {

  warehouses;
  exportWarehouses;
  selectedWarehouses = [];
  warehouseResource = new DataTableResource(this.warehouses);
  wareHouseCount = 0;
  wareHouseFile: File;
  showImportDialog = false;
  showDialog = false;
  mode = 'add';

  wareHouse: WareHouse = {id: 0, name: '', location: '', locationCode: '', workingDays: '', cargoHoldingDays: 0 };

  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };


  @ViewChild(DataTable) wareHouseTable: DataTable;


  constructor(private  serverService: ServerService, private excelService: ExcelService) { }

  ngOnInit() {
   this.serverService.getWarehouses().subscribe(
      (warehouses) => {
        this.warehouses = warehouses;
        this.warehouses = [...this.warehouses];
        this.warehouseResource = new DataTableResource(this.warehouses);
        this.warehouseResource.count().then(count => this.wareHouseCount = count);
        this.reloadWareHouse({'sortBy': 'name', 'offset': '0', 'limit': '15'});
        return this.warehouses;
      },
      error => {
        console.log(error);
        this.warehouses = [];
        console.log('Error getting warehouses!');
      });
  }

  reloadWareHouse(params) {
    this.warehouseResource.query(params).then(warehouses => this.warehouses = warehouses);
  }

  onImport() {
    this.serverService.uploadWarehouse(this.wareHouseFile).subscribe(
      () => {
        console.log('success');
        this.ngOnInit();
      },
      error => {
        console.log(error);
        console.log('Error saving locations!');
      }
    );
    this.showImportDialog = !this.showImportDialog;
  }

  fileChange(file: any) {
    this.wareHouseFile = file.item(0);
  }

  deleteRecord() {
    for (const selected of this.wareHouseTable.selectedRows) {
      this.selectedWarehouses.push(selected.item.id);
    }

    this.serverService.deleteWarehouses(this.selectedWarehouses).subscribe(
      (response) => {
        this.selectedWarehouses = [];
        this.ngOnInit();
      });
  }

  addRecord() {

    this.serverService.addWarehouse(this.wareHouse).subscribe(
      (response) => {
        this.ngOnInit();
      });

    this.resetAllAttributes();

  }

  private resetAllAttributes() {
    this.wareHouse = {id: 0, name: '', location: '', locationCode: '', workingDays: '', cargoHoldingDays: 0 };
    this.mode = 'add';
    this.selectedWarehouses = [];
  }

  exportToExcel() {
    this.exportWarehouses = this.warehouses;
    for (let i = 0; i < this.exportWarehouses.length; i++) {
      delete this.exportWarehouses[i].id;
    }
    this.excelService.exportAsExcelFile(this.exportWarehouses, 'CFS_Warehouse');
    this.ngOnInit();
  }


  editRecord() {
    this.mode = 'edit';
    for (const selected of this.wareHouseTable.selectedRows) {
      this.wareHouse = {
                          id: selected.item.id,
                          name: selected.item.name,
                          location: selected.item.location,
                          locationCode: selected.item.locationCode,
                          workingDays: selected.item.workingDays,
                          cargoHoldingDays: selected.item.cargoHoldingDays
                        };
    }
    this.showDialog = !this.showDialog;
  }

  addWarehouse() {
    this.mode = 'add';
    this.wareHouse = {id: 0, name: '', location: '', locationCode: '', workingDays: '', cargoHoldingDays: 0 };
    this.showDialog = !this.showDialog;
  }
}

export class WareHouse {
  id: number;
  name: string;
  location: string;
  locationCode: string;
  cargoHoldingDays: number;
  workingDays: string;

}
