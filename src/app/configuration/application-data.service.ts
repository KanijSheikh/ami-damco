import { Injectable } from '@angular/core';
import {ServerService} from '../server-service';

@Injectable()
export class ApplicationDataService {

  constructor(private serverService: ServerService) { }

  getModeOfTransports() {
    return this.serverService.getApplicationData('MOT');
  }

  getDamcoOriginServices() {
    return this.serverService.getApplicationData('DamcoOrigin');
  }

  getContainerTypes() {
    return this.serverService.getApplicationData('ContainerType');
  }

}
