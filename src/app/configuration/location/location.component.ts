import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTable, DataTableResource, DataTableTranslations} from 'angular-4-data-table-fix';
import {ServerService} from '../../server-service';
import {ExcelService} from '../excel.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  locations;
  exportLocations;
  selectedLocation = [];
  locationResource = new DataTableResource(this.locations);
  locationCount = 0;
  myLocations: File;
  showImportDialog = false;
  showAddRecordDialog = false;
  showEditDialog = false;

  location;
  locationCode;
  city;
  cityCode;
  country;
  countryCode;

  editId;
  editLocation;
  editLocationCode;
  editCity;
  editCityCode;
  editCountry;
  editCountryCode;




  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };


  @ViewChild(DataTable) locationTable: DataTable;

  ngOnInit() {

    this.serverService.getLocation().subscribe(
      (locations) => {
        this.locations = locations;
        this.locations = [...this.locations];
        this.locationResource = new DataTableResource(this.locations);
        this.locationResource.count().then(count => this.locationCount = count);
        this.reloadLocation({'sortBy': 'location', 'offset': '0', 'limit': '15'});
        return this.locations;
      },
      error => {
        console.log(error);
        this.locations = [];
        console.log('Error getting locations!');
      });
  }

  constructor(private  serverService: ServerService, private excelService: ExcelService) {
  }

  reloadLocation(params) {
    this.locationResource.query(params).then(locations => this.locations = locations);
  }

  onImport() {
    this.serverService.uploadLocations(this.myLocations).subscribe(
      () => {
        console.log('success');
        this.ngOnInit();
      },
      error => {
        console.log(error);
        console.log('Error saving locations!');
      }
    );
    this.showImportDialog = !this.showImportDialog;
  }

  fileChange(locations: any) {
    this.myLocations = locations.item(0);
  }

  deleteLocation() {
    for (let selected of this.locationTable.selectedRows) {
      this.selectedLocation.push(selected.item.id);
    }

    this.serverService.deleteLocation(this.selectedLocation).subscribe(
      (response) => {
        this.selectedLocation = [];
        this.ngOnInit();
      });
  }

  addLocationRecord() {

    const body = '{ "location" : ' + '"' + this.location + '"' + ','
      + '"locationCode" : ' + '"' + this.locationCode + '"' + ','
      + '"city" : ' + '"' + this.city + '"' + ','
      + '"cityCode" : ' + '"' + this.cityCode + '"' + ','
      + '"country" : ' + '"' + this.country  + '"' +  ','
      + '"countryCode" : ' + '"' + this.countryCode + '"'
      + '}';

    this.serverService.addLocation(body).subscribe(
      (response) => {
        this.ngOnInit();
      });

    this.resetAllAttributes();

  }

  private resetAllAttributes() {
    this.location = '';
    this.locationCode = '';
    this.city = '';
    this.cityCode = '';
    this.country = '';
    this.countryCode = '';
  }

  exportToExcel() {
    this.exportLocations = this.locations
    for (var i = 0; i < this.exportLocations.length; i++) {
      delete this.exportLocations[i].id;
    }
    this.excelService.exportAsExcelFile(this.exportLocations, 'locations');
    this.ngOnInit();
  }


  editRecord() {

    for (let selected of this.locationTable.selectedRows) {
      this.editId = selected.item.id;
      this.editLocation = selected.item.location;
      this.editLocationCode = selected.item.locationCode;
      this.editCity = selected.item.city;
      this.editCityCode = selected.item.cityCode;
      this.editCountry = selected.item.country;
      this.editCountryCode = selected.item.countryCode;
      this.selectedLocation.push(selected.item.id);
    }
    this.showEditDialog = !this.showEditDialog;
  }

  editLocationRecord() {
    const body = '{ ' + '"id" : ' + '"' + this.editId + '"' + ','
      + '"location" : ' + '"' + this.editLocation + '"' + ','
      + '"locationCode" : ' + '"' + this.editLocationCode + '"' + ','
      + '"city" : ' + '"' + this.editCity + '"' + ','
      + '"cityCode" : ' + '"' + this.editCityCode + '"' + ','
      + '"country" : ' + '"' + this.editCountry  + '"' +  ','
      + '"countryCode" : ' + '"' + this.editCountryCode + '"'
      + '}';

    this.serverService.addLocation(body).subscribe(
      (response) => {
        this.ngOnInit();
      });
  }

}


