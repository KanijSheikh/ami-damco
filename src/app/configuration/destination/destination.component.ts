import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTable, DataTableResource, DataTableTranslations} from 'angular-4-data-table-fix';
import {ServerService} from '../../server-service';
import {ExcelService} from '../excel.service';

@Component({
  selector: 'app-destination',
  templateUrl: './destination.component.html',
  styleUrls: ['./destination.component.css']
})
export class DestinationComponent implements OnInit {

  destinations;
  exportDestinations;
  selectedDestination = [];
  destinationResource = new DataTableResource(this.destinations);
  destinationCount = 0;
  myDestination: File;
  showImportDialog = false;
  showAddRecordDialog = false;
  showEditDialog = false;

  destinationName;
  dischargePort;
  transitTime;


  editId;
  editDestinationName;
  editDischargePort;
  editTransitTime;





  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };


  @ViewChild(DataTable) destinationTable: DataTable;

  ngOnInit() {

    this.serverService.getDestination().subscribe(
      (destinations) => {
        this.destinations = destinations;
        this.destinations = [...this.destinations];
        this.destinationResource = new DataTableResource(this.destinations);
        this.destinationResource.count().then(count => this.destinationCount = count);
        this.reloadDestination({'sortBy': 'destinationName', 'offset': '0', 'limit': '15'});
        return this.destinations;
      },
      error => {
        console.log(error);
        this.destinations = [];
        console.log('Error getting destinations!');
      });
  }

  constructor(private  serverService: ServerService, private excelService: ExcelService) {
  }

  reloadDestination(params) {
    this.destinationResource.query(params).then(destinations => this.destinations = destinations);
  }

  onImport() {
    this.serverService.uploadDestination(this.myDestination).subscribe(
      () => {
        console.log('success');
        this.ngOnInit();
      },
      error => {
        console.log(error);
        console.log('Error saving destinations!');
      }
    );
    this.showImportDialog = !this.showImportDialog;
  }

  fileChange(destinations: any) {
    this.myDestination = destinations.item(0);
  }

  deleteDestination() {
    for (let selected of this.destinationTable.selectedRows) {
      this.selectedDestination.push(selected.item.id);
    }

    this.serverService.deleteDestination(this.selectedDestination).subscribe(
      (response) => {
        this.selectedDestination = [];
        this.ngOnInit();
      });
  }

  addDestinationRecord() {

    const body = '{ "destinationName" : ' + '"' + this.destinationName + '"' + ','
      + '"dischargePort" : ' + '"' + this.dischargePort + '"' + ','
      + '"transitTime" : '  + this.transitTime
      + '}';

    this.serverService.addDestination(body).subscribe(
      (response) => {
        this.ngOnInit();
      });

    this.resetAllAttributes();
  }

  private resetAllAttributes() {
    this.destinationName = '';
    this.dischargePort = '';
    this.transitTime = '';
  }

  exportToExcel() {
    this.exportDestinations = this.destinations
    for (var i = 0; i < this.exportDestinations.length; i++) {
      delete this.exportDestinations[i].id;
    }
    this.excelService.exportAsExcelFile(this.exportDestinations, 'destinations');
    this.ngOnInit();
  }


  editRecord() {

    for (let selected of this.destinationTable.selectedRows) {
      this.editId = selected.item.id;
      this.editDestinationName = selected.item.destinationName;
      this.editDischargePort = selected.item.dischargePort;
      this.editTransitTime = selected.item.transitTime;
      this.selectedDestination.push(selected.item.id);
    }
    this.showEditDialog = !this.showEditDialog;
  }

  editDestinationRecord() {
    const body = '{ ' + '"id" : ' + '"' + this.editId + '"' + ','
      + '"destinationName" : ' + '"' + this.editDestinationName + '"' + ','
      + '"dischargePort" : ' + '"' + this.editDischargePort + '"' + ','
      + '"transitTime" : '  + this.editTransitTime
      + '}';

    this.serverService.addDestination(body).subscribe(
      (response) => {
        this.ngOnInit();
      });
  }

}
