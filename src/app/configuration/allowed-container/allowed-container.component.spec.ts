import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllowedContainerComponent } from './allowed-container.component';

describe('AllowedContainerComponent', () => {
  let component: AllowedContainerComponent;
  let fixture: ComponentFixture<AllowedContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllowedContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllowedContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
