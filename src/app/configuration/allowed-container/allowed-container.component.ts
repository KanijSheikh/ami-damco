import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTable, DataTableResource, DataTableTranslations} from 'angular-4-data-table-fix';
import {ExcelService} from '../excel.service';
import {ServerService} from '../../server-service';

@Component({
  selector: 'app-allowed-container',
  templateUrl: './allowed-container.component.html',
  styleUrls: ['./allowed-container.component.css']
})
export class AllowedContainerComponent implements OnInit {

  allowedContainers;
  exportAllowedContainers;
  selectedContainers = [];
  allowedContainersResource = new DataTableResource(this.allowedContainers);
  allowedContainersCount = 0;
  file: File;
  showImportDialog = false;
  showDialog = false;
  mode = 'add';

  allowedContainer: AllowedContainer = {id: 0, origin: '', destination: '', allow20Dry: '',
                                          allow40Dry: '', allow40High: '', allow40Reef: '', allow45High: '' };

  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };


  @ViewChild(DataTable) dataTable: DataTable;


  constructor(private  serverService: ServerService, private excelService: ExcelService) { }

  ngOnInit() {
    this.serverService.getAllowedContainers().subscribe(
      (allowedContainers) => {
        this.allowedContainers = allowedContainers;
        this.allowedContainers = [...this.allowedContainers];
        this.allowedContainersResource = new DataTableResource(this.allowedContainers);
        this.allowedContainersResource.count().then(count => this.allowedContainersCount = count);
        this.reload({'sortBy': 'origin', 'offset': '0', 'limit': '15'});
        return this.allowedContainers;
      },
      error => {
        console.log(error);
        this.allowedContainers = [];
        console.log('Error getting allowed containers!');
      });
  }

  reload(params) {
    this.allowedContainersResource.query(params).then(allowedContainers => this.allowedContainers = allowedContainers);
  }

  onImport() {
    this.serverService.uploadAllowedContainers(this.file).subscribe(
      () => {
        console.log('success');
        this.ngOnInit();
      },
      error => {
        console.log(error);
        console.log('Error saving allowed containers!');
      }
    );
    this.showImportDialog = !this.showImportDialog;
  }

  fileChange(file: any) {
    this.file = file.item(0);
  }

  deleteRecord() {
    for (const selected of this.dataTable.selectedRows) {
      this.selectedContainers.push(selected.item.id);
    }

    this.serverService.deleteAllowedContainers(this.selectedContainers).subscribe(
      (response) => {
        this.selectedContainers = [];
        this.ngOnInit();
      });
  }

  addRecord() {

    this.serverService.addAllowedContainer(this.allowedContainer).subscribe(
      (response) => {
        this.ngOnInit();
      });

    this.resetAllAttributes();

  }

  private resetAllAttributes() {
    this.allowedContainer = {id: 0, origin: '', destination: '', allow20Dry: '',
                              allow40Dry: '', allow40High: '', allow40Reef: '', allow45High: '' };
    this.mode = 'add';
    this.selectedContainers = [];
  }

  exportToExcel() {
    this.exportAllowedContainers = this.allowedContainers;
    for (let i = 0; i < this.exportAllowedContainers.length; i++) {
      delete this.exportAllowedContainers[i].id;
    }
    this.excelService.exportAsExcelFile(this.exportAllowedContainers, 'Allowed_Containers');
    this.ngOnInit();
  }


  editRecord() {
    this.mode = 'edit';
    for (const selected of this.dataTable.selectedRows) {
      this.allowedContainer = {
        id: selected.item.id,
        origin: selected.item.origin,
        destination: selected.item.destination,
        allow20Dry: selected.item.allow20Dry,
        allow40Dry: selected.item.allow40Dry,
        allow40High: selected.item.allow40High,
        allow40Reef: selected.item.allow40Reef,
        allow45High: selected.item.allow45High
      };
    }
    this.showDialog = !this.showDialog;
  }

  addContainer() {
    this.mode = 'add';
    this.allowedContainer = {id: 0, origin: '', destination: '', allow20Dry: '',
                              allow40Dry: '', allow40High: '', allow40Reef: '', allow45High: '' };
    this.showDialog = !this.showDialog;
  }

}

export class AllowedContainer {
  id: number;
  origin: string;
  destination: string;
  allow20Dry: string;
  allow40Dry: string;
  allow40High: string;
  allow40Reef: string;
  allow45High: string;

}
