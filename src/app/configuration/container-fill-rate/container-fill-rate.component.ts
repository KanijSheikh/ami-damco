import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTable, DataTableResource, DataTableTranslations} from 'angular-4-data-table-fix';
import {ServerService} from '../../server-service';
import {ExcelService} from '../excel.service';

@Component({
  selector: 'app-container-fill-rate',
  templateUrl: './container-fill-rate.component.html',
  styleUrls: ['./container-fill-rate.component.css']
})
export class ContainerFillRateComponent implements OnInit {

  containerFillRates;
  exportContainerFillRates;
  selectedContainerFillRate = [];
  containerFillRateResource = new DataTableResource(this.containerFillRates);
  containerCount = 0;
  myContainerFillRate: File;
  showImportDialog = false;
  showAddRecordDialog = false;
  showEditDialog = false;

  productType;
  containerSize;
  minCbm;
  maxCbm;
  minKgs;
  maxKgs;
  origin;
  destination;


  editId;
  editProductType;
  editContainerSize;
  editMinCbm;
  editMaxCbm;
  editMinKgs;
  editMaxKgs;
  editOrigin;
  editDestination;





  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };


  @ViewChild(DataTable) containerTable: DataTable;

  ngOnInit() {

    this.serverService.getContainerFillRate().subscribe(
      (containers) => {
        this.containerFillRates = containers;
        this.containerFillRates = [...this.containerFillRates];
        this.containerFillRateResource = new DataTableResource(this.containerFillRates);
        this.containerFillRateResource.count().then(count => this.containerCount = count);
        this.reloadContainerFillRate({'sortBy': 'productType', 'offset': '0', 'limit': '15'});
        return this.containerFillRates;
      },
      error => {
        console.log(error);
        this.containerFillRates = [];
        console.log('Error getting Conatiner Fill Rate!');
      });
  }

  constructor(private  serverService: ServerService, private excelService: ExcelService) {
  }

  reloadContainerFillRate(params) {
    this.containerFillRateResource.query(params).then(containers => this.containerFillRates = containers);
  }

  onImport() {
    this.serverService.uploadContainerFillRate(this.myContainerFillRate).subscribe(
      () => {
        console.log('success');
        this.ngOnInit();
      },
      error => {
        console.log(error);
        console.log('Error saving container fill rate!');
      }
    );
    this.showImportDialog = !this.showImportDialog;
  }

  fileChange(containers: any) {
    this.myContainerFillRate = containers.item(0);
  }

  deleteContainerFillRate() {
    for (let selected of this.containerTable.selectedRows) {
      this.selectedContainerFillRate.push(selected.item.id);
    }

    this.serverService.deleteContainerFillRate(this.selectedContainerFillRate).subscribe(
      (response) => {
        this.selectedContainerFillRate = [];
        this.ngOnInit();
      });
  }

  addContainerFillRateRecord() {

    const body = '{ "productType" : ' + '"' + this.productType + '"' + ','
      + '"containerSize" : ' + '"' + this.containerSize + '"' + ','
      + '"minCbm" : '  + this.minCbm + ','
      + '"maxCbm" : '  + this.maxCbm + ','
      + '"minKgs" : '  + this.minKgs + ','
      + '"maxKgs" : '  + this.maxKgs + ','
      + '"origin" : ' + '"' + this.origin + '"' + ','
      + '"destination" : ' + '"' + this.destination + '"'
      + '}';

    this.serverService.addContainerFillRate(body).subscribe(
      (response) => {
        this.ngOnInit();
      });

    this.resetAllAttributes();

  }

  private resetAllAttributes() {
    this.productType = '';
    this.containerSize = '';
    this.minCbm = '';
    this.maxCbm = '';
    this.minKgs = '';
    this.origin = '';
    this.destination = '';
  }

  exportToExcel() {
    this.exportContainerFillRates = this.containerFillRates
    for (var i = 0; i < this.exportContainerFillRates.length; i++) {
      delete this.exportContainerFillRates[i].id;
    }
    this.excelService.exportAsExcelFile(this.exportContainerFillRates, 'containerFillRates');
    this.ngOnInit();
  }


  editRecord() {

    for (let selected of this.containerTable.selectedRows) {
      this.editId = selected.item.id;
      this.editProductType = selected.item.productType;
      this.editContainerSize = selected.item.containerSize;
      this.editMinCbm = selected.item.minCbm;
      this.editMaxCbm = selected.item.maxCbm;
      this.editMinKgs = selected.item.minKgs;
      this.editMaxKgs = selected.item.maxKgs;
      this.editOrigin = selected.item.origin;
      this.editDestination = selected.item.destination;
      this.selectedContainerFillRate.push(selected.item.id);
    }
    this.showEditDialog = !this.showEditDialog;
  }

  editContainerFillRateRecord() {
    const body = '{ ' + '"id" : ' + '"' + this.editId + '"' + ','
      + '"productType" : ' + '"' + this.editProductType + '"' + ','
      + '"containerSize" : ' + '"' + this.editContainerSize + '"' + ','
      + '"minCbm" : '  + this.editMinCbm + ','
      + '"maxCbm" : '  + this.editMaxCbm + ','
      + '"minKgs" : '  + this.editMinKgs + ','
      + '"maxKgs" : '  + this.editMaxKgs + ','
      + '"origin" : ' + '"' + this.editOrigin + '"' + ','
      + '"destination" : ' + '"' + this.editDestination + '"'
      + '}';


    this.serverService.addContainerFillRate(body).subscribe(
      (response) => {
        this.ngOnInit();
      });
  }

}
