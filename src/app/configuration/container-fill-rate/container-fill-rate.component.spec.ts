import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerFillRateComponent } from './container-fill-rate.component';

describe('ContainerFillRateComponent', () => {
  let component: ContainerFillRateComponent;
  let fixture: ComponentFixture<ContainerFillRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerFillRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerFillRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
