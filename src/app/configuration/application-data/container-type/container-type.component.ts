import { Component, OnInit } from '@angular/core';
import {ServerService} from "../../../server-service";

@Component({
  selector: 'app-container-type',
  templateUrl: './container-type.component.html',
  styleUrls: ['./container-type.component.css']
})
export class ContainerTypeComponent implements OnInit {

  containerTypes: any;

  newType: {containerType: string, volume: number} = {containerType: '', volume: null};
  index = -1;
  operation = 'add';

  constructor(private serverService: ServerService) { }

  ngOnInit() {

    this.serverService.getContainerTypes().subscribe(
      (response) => {
        this.containerTypes = response;
      },
      (error) => {
        this.containerTypes = [];
      }
    );
  }

  onAdd() {
    if (this.newType && this.newType.containerType !== '') {
      const index = this.containerTypes.findIndex(type => type.containerType.toUpperCase() === this.newType.containerType.toUpperCase());
      if (index === -1) {
        if (this.index === -1) {
          this.containerTypes.push(this.newType);
        } else {
          this.containerTypes[this.index] = this.newType;
        }
      }else {
        this.containerTypes[index] = this.newType;
      }

      this.newType = {containerType: '', volume: null};
      this.index = -1;
      this.operation = 'add';
      this.onSave();
    }
  }

  onEdit(i) {
    this.index = i;
    this.newType = Object.assign({}, this.containerTypes[i]);
    this.operation = 'edit';
  }

  onRemove(i) {
    if (i > -1) {
      this.containerTypes.splice(i, 1);
    }
    this.newType = {containerType: '', volume: null};
    this.index = -1;
    this.operation = 'add';
    this.onSave();
  }

  onSave() {
    this.serverService.saveContainerTypes(this.containerTypes).subscribe(
      (response) => {
        this.ngOnInit();
      }
    );
  }



}
