import { Component, OnInit } from '@angular/core';
import {ServerService} from '../../server-service';
import {ActivatedRoute, Params, Router} from "@angular/router";

@Component({
  selector: 'app-application-data',
  templateUrl: './application-data.component.html',
  styleUrls: ['./application-data.component.css']
})
export class ApplicationDataComponent implements OnInit {

  type;
  configValues: any;

  newMode: string;
  index = -1;
  operation = 'add';

  validValues = ['MOT', 'DamcoOrigin', 'CarrierOrigin', 'CarrierDestination'];

  constructor(private serverService: ServerService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {

    this.route.params
      .subscribe(
        (params: Params) => {
          this.type = params['type'];
          if (!this.validValues.includes(this.type)) {
            this.router.navigate(['../', 'MOT'], {relativeTo: this.route});
          }
          this.serverService.getApplicationData(this.type).subscribe(
            (response) => {
              this.configValues = response;
              this.configValues.sort();
            },
            (error) => {
              this.configValues = [];
            }
          );
        }
      );
  }

  onAdd() {
    if (this.newMode && this.newMode !== '') {
      const index = this.configValues.findIndex(mode => mode.toUpperCase() === this.newMode.toUpperCase());
      if (index === -1) {
        if (this.index === -1) {
          this.configValues.push(this.newMode);
        } else {
          this.configValues[this.index] = this.newMode;
        }
        this.configValues.sort();
      }else {
        this.configValues[index] = this.newMode;
      }

      this.newMode = '';
      this.index = -1;
      this.operation = 'add';
      this.onSave();
    }
  }

  onEdit(i) {
    this.index = i;
    this.newMode = this.configValues[i];
    this.operation = 'edit';
  }

  onRemove(i) {
    if (i > -1) {
      this.configValues.splice(i, 1);
    }
    this.newMode = '';
    this.index = -1;
    this.operation = 'add';
    this.onSave();
  }

  onSave() {
    this.serverService.saveApplicationData(this.type, this.configValues).subscribe(
      (response) => {
        this.ngOnInit();
      }
    );
  }


}
