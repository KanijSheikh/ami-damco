import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTable, DataTableResource, DataTableTranslations} from "angular-4-data-table-fix";
import {ServerService} from "../../server-service";
import {ExcelService} from "../excel.service";

@Component({
  selector: 'app-origin',
  templateUrl: './origin.component.html',
  styleUrls: ['./origin.component.css']
})
export class OriginComponent implements OnInit {

  origins;
  exportOrigin;
  selectedOrigin = [];
  originResource = new DataTableResource(this.origins);
  originCount = 0;
  myOrigin: File;
  showImportDialog = false;
  showAddRecordDialog = false;
  showEditDialog = false;

  originName;
  placeOfReceipt;
  cfsWarehouse;
  loadingPort;


  editId;
  editOriginName;
  editPlaceOfReceipt;
  editCfsWareHouse;
  editLoadingPort;






  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };


  @ViewChild(DataTable) originTable: DataTable;

  ngOnInit() {

    this.serverService.getOrigin().subscribe(
      (origins) => {
        this.origins = origins;
        this.origins = [...this.origins];
        this.originResource = new DataTableResource(this.origins);
        this.originResource.count().then(count => this.originCount = count);
        this.reloadOrigin({'sortBy': 'OriginName', 'offset': '0', 'limit': '15'});
        return this.origins;
      },
      error => {
        console.log(error);
        this.origins = [];
        console.log('Error getting origin!');
      });
  }

  constructor(private  serverService: ServerService, private excelService: ExcelService) {
  }

  reloadOrigin(params) {
    this.originResource.query(params).then(origins => this.origins = origins);
  }

  onImport() {
    this.serverService.uploadOrigin(this.myOrigin).subscribe(
      () => {
        console.log('success');
        this.ngOnInit();
      },
      error => {
        console.log(error);
        console.log('Error saving origin!');
      }
    );
    this.showImportDialog = !this.showImportDialog;
  }

  fileChange(origins: any) {
    this.myOrigin = origins.item(0);
  }

  deleteOrigin() {
    for (let selected of this.originTable.selectedRows) {
      this.selectedOrigin.push(selected.item.id);
    }

    this.serverService.deleteOrigin(this.selectedOrigin).subscribe(
      (response) => {
        this.selectedOrigin = [];
        this.ngOnInit();
      });
  }

  addOriginRecord() {

    const body = '{ "originName" : ' + '"' + this.originName + '"' + ','
      + '"placeOfReceipt" : ' + '"' + this.placeOfReceipt + '"' + ','
      + '"cfsWarehouse" : ' + '"' + this.cfsWarehouse + '"' + ','
      + '"loadingPort" : ' + '"' + this.loadingPort + '"'
      + '}';

    this.serverService.addOrigin(body).subscribe(
      (response) => {
        this.ngOnInit();
      });

    this.resetAllAttributes();
  }

  private resetAllAttributes() {
    this.originName = '';
    this.placeOfReceipt = '';
    this.cfsWarehouse = '';
    this.loadingPort = '';
  }

  exportToExcel() {
    this.exportOrigin = this.origins
    for (var i = 0; i < this.exportOrigin.length; i++) {
      delete this.exportOrigin[i].id;
    }
    this.excelService.exportAsExcelFile(this.exportOrigin, 'origins');
    this.ngOnInit();
  }


  editRecord() {

    for (let selected of this.originTable.selectedRows) {
      this.editId = selected.item.id;
      this.editOriginName = selected.item.originName;
      this.editPlaceOfReceipt = selected.item.placeOfReceipt;
      this.editCfsWareHouse = selected.item.cfsWarehouse;
      this.editLoadingPort = selected.item.loadingPort;
      this.selectedOrigin.push(selected.item.id);
    }
    this.showEditDialog = !this.showEditDialog;
  }

  editOriginRecord() {
    const body = '{ ' + '"id" : ' + '"' + this.editId + '"' + ','
      + '"originName" : ' + '"' + this.editOriginName + '"' + ','
      + '"placeOfReceipt" : ' + '"' + this.editPlaceOfReceipt + '"' + ','
      + '"cfsWarehouse" : ' + '"' + this.editCfsWareHouse + '"' + ','
      + '"loadingPort" : ' + '"' + this.editLoadingPort + '"'
      + '}';

    this.serverService.addOrigin(body).subscribe(
      (response) => {
        this.ngOnInit();
      });
  }

}
