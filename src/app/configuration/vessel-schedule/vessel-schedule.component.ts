import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTable, DataTableResource, DataTableTranslations} from 'angular-4-data-table-fix';
import {ServerService} from '../../server-service';
import {ExcelService} from '../excel.service';
import {IMyOptions} from "mydatepicker";

@Component({
  selector: 'app-vessel-schedule',
  templateUrl: './vessel-schedule.component.html',
  styleUrls: ['./vessel-schedule.component.css']
})
export class VesselScheduleComponent implements OnInit {

  vesselSchedules;
  exportVesselSchedules;
  selectedVesselSchedule = [];
  vesselScheduleResource = new DataTableResource(this.vesselSchedules);
  vesselScheduleCount = 0;
  myVesselSchedules: File;
  showImportDialog = false;
  showAddRecordDialog = false;
  showEditDialog = false;

  modeOfTransport;
  loadingPort;
  dischargePort;
  carrier;
  loop;
  transitTime;
  etd;
  eta;
  activationDate;
  deactivationDate;
  cuttOffDay;
  twentyDryPerWeek;
  fourtyDryPerWeek;
  fourtyDryHcPerWeek;
  fourtyFiveDryPerWeek;

  editId;
  editModeOfTransport;
  editLoadingPort;
  editDischargePort;
  editCarrier;
  editLoop;
  editTransitTime;
  editEtd;
  editEta;
  editActivationDate;
  editDeactivationDate;
  editCuttOffDay;
  editTwentyDryPerWeek;
  editFourtyDryPerWeek;
  editFourtyDryHcPerWeek;
  editFourtyFiveDryPerWeek;


  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'yyyy-mm-dd',
  };


  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };


  @ViewChild(DataTable) vesselScheduleTable: DataTable;

  ngOnInit() {

    this.serverService.getVesselSchedule().subscribe(
      (vesselSchedules) => {
        this.vesselSchedules = vesselSchedules;
        this.vesselSchedules = [...this.vesselSchedules];
        this.vesselScheduleResource = new DataTableResource(this.vesselSchedules);
        this.vesselScheduleResource.count().then(count => this.vesselScheduleCount = count);
        this.reloadVesselSchedule({'sortBy': 'modeOfTransport', 'offset': '0', 'limit': '15'});
        return this.vesselSchedules;
      },
      error => {
        console.log(error);
        this.vesselSchedules = [];
        console.log('Error getting vesselSchedules!');
      });
  }

  constructor(private  serverService: ServerService, private excelService: ExcelService) {
  }

  reloadVesselSchedule(params) {
    this.vesselScheduleResource.query(params).then(vesselSchedules => this.vesselSchedules = vesselSchedules);
  }

  onImport() {
    this.serverService.uploadVesselSchedule(this.myVesselSchedules).subscribe(
      () => {
        console.log('success');
        this.ngOnInit();
      },
      error => {
        console.log(error);
        console.log('Error saving vesselSchedules!');
      }
    );
    this.showImportDialog = !this.showImportDialog;
  }

  fileChange(vesselSchedules: any) {
    this.myVesselSchedules = vesselSchedules.item(0);
  }

  deleteVesselSchedule() {
    for (let selected of this.vesselScheduleTable.selectedRows) {
      this.selectedVesselSchedule.push(selected.item.id);
    }

    this.serverService.deleteVesselSchedule(this.selectedVesselSchedule).subscribe(
      (response) => {
        this.selectedVesselSchedule = [];
        this.ngOnInit();
      });
  }

  addVesselScheduleRecord() {

    const body = '{ "modeOfTransport" : ' + '"' + this.modeOfTransport + '"' + ','
      + '"loadingPort" : ' + '"' + this.loadingPort + '"' + ','
      + '"dischargePort" : ' + '"' + this.dischargePort + '"' + ','
      + '"carrier" : ' + '"' + this.carrier + '"' + ','
      + '"loop" : ' + '"' + this.loop  + '"' +  ','
      + '"transitTime" : '  + this.transitTime  + ','
      + '"etd" : ' + '"' + this.etd  + '"' +  ','
      + '"eta" : ' + '"' + this.eta  + '"' +  ','
      + '"activationDate" : ' + '"' + this.activationDate.formatted  + '"' +  ','
      + '"deactivationDate" : ' + '"' + this.deactivationDate.formatted  + '"' +  ','
      + '"cuttOffDay" : ' + this.cuttOffDay +  ','
      + '"twentyDryPerWeek" : ' + this.twentyDryPerWeek  +  ','
      + '"fourtyDryPerWeek" : ' + this.fourtyDryPerWeek  +  ','
      + '"fourtyDryHcPerWeek" : ' + this.fourtyDryHcPerWeek  +  ','
      + '"fourtyFiveDryPerWeek" : ' + this.fourtyFiveDryPerWeek
      + '}';


    console.log(body);

    this.serverService.addVesselSchedule(body).subscribe(
      (response) => {
        this.ngOnInit();
      });

    this.resetAllAttributes();

  }

  private resetAllAttributes() {
    this.modeOfTransport = '';
    this.loadingPort = '';
    this.dischargePort = '';
    this.carrier = '';
    this.loop = '';
    this.transitTime = '';
    this.etd = '';
    this.eta = '';
    this.activationDate = '';
    this.deactivationDate = '';
    this.cuttOffDay = '';
    this.twentyDryPerWeek = '';
    this.fourtyDryPerWeek = '';
    this.fourtyDryHcPerWeek = '';
    this.fourtyFiveDryPerWeek = '';
  }

  exportToExcel() {
    this.exportVesselSchedules = this.vesselSchedules
    for (var i = 0; i < this.exportVesselSchedules.length; i++) {
      delete this.exportVesselSchedules[i].id;
    }
    this.excelService.exportAsExcelFile(this.exportVesselSchedules, 'vesselSchedules');
    this.ngOnInit();
  }


  editRecord() {

    for (let selected of this.vesselScheduleTable.selectedRows) {
      this.editId = selected.item.id;
      this.editModeOfTransport = selected.item.modeOfTransport;
      this.editLoadingPort = selected.item.loadingPort;
      this.editDischargePort = selected.item.dischargePort;
      this.editCarrier = selected.item.carrier;
      this.editLoop = selected.item.loop;
      this.editTransitTime = selected.item.transitTime;
      this.editEtd = selected.item.etd;
      this.editEta = selected.item.eta;
      this.editActivationDate = selected.item.activationDate;
      this.editDeactivationDate = selected.item.deactivationDate;
      this.editCuttOffDay = selected.item.cuttOffDay;
      this.editTwentyDryPerWeek = selected.item.twentyDryPerWeek;
      this.editFourtyDryPerWeek = selected.item.fourtyDryPerWeek;
      this.editFourtyDryHcPerWeek = selected.item.fourtyDryHcPerWeek;
      this.editFourtyFiveDryPerWeek = selected.item.fourtyFiveDryPerWeek;

      this.selectedVesselSchedule.push(selected.item.id);
    }
    this.showEditDialog = !this.showEditDialog;
  }

  editVesselScheduleRecord() {

    const body = '{ ' + '"id" : ' + '"' + this.editId + '"' + ','
      + '"modeOfTransport" : ' + '"' + this.editModeOfTransport + '"' + ','
      + '"loadingPort" : ' + '"' + this.editLoadingPort + '"' + ','
      + '"dischargePort" : ' + '"' + this.editDischargePort + '"' + ','
      + '"carrier" : ' + '"' + this.editCarrier + '"' + ','
      + '"loop" : ' + '"' + this.editLoop  + '"' +  ','
      + '"transitTime" : ' + this.editTransitTime  +  ','
      + '"etd" : ' + '"' + this.editEtd  + '"' +  ','
      + '"eta" : ' + '"' + this.editEta  + '"' +  ','
      + '"activationDate" : ' + '"' + this.editActivationDate.formatted  + '"' +  ','
      + '"deactivationDate" : ' + '"' + this.editDeactivationDate.formatted  + '"' +  ','
      + '"cuttOffDay" : ' + this.editCuttOffDay +  ','
      + '"twentyDryPerWeek" : ' + this.editTwentyDryPerWeek  +  ','
      + '"fourtyDryPerWeek" : ' + this.editFourtyDryPerWeek  +  ','
      + '"fourtyDryHcPerWeek" : ' + this.editFourtyDryHcPerWeek  +  ','
      + '"fourtyFiveDryPerWeek" : ' + this.editFourtyFiveDryPerWeek
      + '}';

    this.serverService.addVesselSchedule(body).subscribe(
      (response) => {
        this.ngOnInit();
      });
  }

}
