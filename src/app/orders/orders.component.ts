import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTable, DataTableResource, DataTableTranslations} from 'angular-4-data-table-fix';
import {ServerService} from '../server-service';
import {IMyOptions} from 'mydatepicker';
import {CountryDataService} from '../country-data-service';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders;
  ordersResource = new DataTableResource(this.orders);
  Name: string;
  myFile: File;

  showImportDialog = false;
  ordersCount = 0;
  showFilter = false;
  translations = <DataTableTranslations>{
    indexColumn: 'Index column',
    expandColumn: 'Expand column',
    selectColumn: 'Select column',
    paginationLimit: 'Max results',
    paginationRange: 'Result range'
  };
  id = 0;

  filterVal = {status: '', origin: '', bookDate: '', edd: '', destination: '', soNumber: ''};

  @ViewChild(DataTable) ordersTable;

  fileType;

  fileTypes = ['CIR-EUDS', 'CIR-EUDRS', 'CIR-CHINA', 'CIR-APLA', 'SO-PO-Report', 'SO-Cancel-Report'];

  public myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy'
  };

  originValues;
  originCities: string[] = [];
  destinationValues;
  destinationCities: string[] = [];
  statuses: string [] = ['Not Planned', 'Optimized', 'Exception'];


  ngOnInit() {

    this.serverService.getDestination().subscribe(
      (destinations) => {
        this.destinationValues = destinations;
        for (let destination of this.destinationValues) {
          if (!(this.destinationCities.includes(destination.destinationName))) {
            this.destinationCities.push(destination.destinationName);
            this.destinationCities.sort();
          }

        }
      }
    );

    this.serverService.getOrigin().subscribe(
      (origins) => {
        this.originValues = origins;
        for (let origin of this.originValues) {
          if (!(this.originCities.includes(origin.originName))) {
            this.originCities.push(origin.originName);
            this.originCities.sort();
          }
        }
      }
    );

    this.serverService.getPurchaseOrders().subscribe(
      (orders) => {
        this.orders = orders;
        this.orders = [...this.orders];
        this.ordersResource = new DataTableResource(this.orders);
        this.ordersResource.count().then(count => this.ordersCount = count);
        this.reloadorders({'sortBy': 'expectedDeliveryDate', 'offset': '0', 'limit': '15'});
        return this.orders;
      }
    );
  }

  constructor(private serverService: ServerService, private countryDataService: CountryDataService) {
  }

  reloadorders(params) {
    if (this.isFilterApplied()) {
      const val = this.filterVal;
      this.ordersResource.query(params, this.filter(val)).then(orders => {
        this.orders = orders;
      });

    } else {

      this.ordersResource.query(params).then(orders => this.orders = orders);
      this.ordersResource.count().then(count => this.ordersCount = count);
    }
  }

  isFilterApplied() {
    return this.filterVal.soNumber !== '' ||
      this.filterVal.status !== '' ||
      this.filterVal.origin !== '' ||
      this.filterVal.bookDate !== '' ||
      this.filterVal.edd !== '' ||
      this.filterVal.destination !== '';
  }


  filter(val) {

    return function (element) {
      return (val.status === '' || element.loadPlanStatus.toUpperCase().indexOf(val.status.toUpperCase()) > -1 ) &&
        (val.origin === '' || element.loadPortCity.toUpperCase().indexOf(val.origin.toUpperCase()) > -1 ) &&
        (val.destination === '' || element.podCity.toUpperCase().indexOf(val.destination.toUpperCase()) > -1 ) &&
        (val.edd === '' || val.edd.formatted === element.expectedDeliveryDate ) &&
        (val.bookDate === '' || val.bookDate.formatted === element.bookDate ) &&
        (val.soNumber === '' || element.poNumber.toUpperCase().indexOf(val.soNumber.toUpperCase()) > -1
          || element.soNumber.toUpperCase().indexOf(val.soNumber.toUpperCase()) > -1 ||
          ( element.containerNumber && element.containerNumber.toUpperCase().indexOf(val.soNumber.toUpperCase()) > -1) ||
          (element.loadPlanNumber && element.loadPlanNumber.toUpperCase().indexOf(val.soNumber.toUpperCase()) > -1));
    };
  }

  cellColor(car) {
    return 'rgb(255, 255,' + (155 + Math.floor(100 - ((car.rating - 8.7) / 1.3) * 100)) + ')';
  }


  onClear() {
    this.filterVal = {status: '', origin: '', bookDate: '', edd: '', destination: '', soNumber: ''};
    this.filterContent();
  }


  fileChange(files: any) {
    this.myFile = files.item(0);
  }

  onImport() {

    this.serverService.uploadPurchaseOrder(this.myFile, this.fileType).subscribe(
      () => {
        console.log('success');
        this.ngOnInit();
      },
      error => {
        console.log(error);
        console.log('Error saving food!');
      }
    );
    this.showImportDialog = !this.showImportDialog;
  }


  filterContent() {
    const val = this.filterVal;
    if (!val.bookDate) {
      val.bookDate = '';
    }
    if (!val.edd) {
      val.edd = '';
    }
    this.ordersResource.query({}).then(orders => this.ordersCount = orders.filter(this.filter(val)).length);

    this.reloadorders({'sortBy': 'expectedDeliveryDate', 'offset': '0', 'limit': 15});


  }
}
