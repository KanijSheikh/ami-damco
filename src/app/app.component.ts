import {Component, OnDestroy} from '@angular/core';
import {NgLoadingSpinnerService} from "./loader/loader.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy{
  title = 'app';

  public show: boolean;

  public constructor(
    private progress: NgLoadingSpinnerService
  ) {
    this.progress.state.subscribe(
      (response: boolean) => {
        this.show = response;
      }
    );
  }
  public ngOnDestroy() {
    this.progress.state.unsubscribe();
  }
}
